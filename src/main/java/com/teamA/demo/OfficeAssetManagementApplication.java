package com.teamA.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfficeAssetManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfficeAssetManagementApplication.class, args);
	}

}
