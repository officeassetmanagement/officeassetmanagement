
package com.teamA.demo.controller;


import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.service.AdminService;
import com.teamA.demo.service.ProjectManagerService;


@Controller
@RequestMapping("/admin")
public class AdminController {
	public String error="";
	private AdminService adminService;
	private ProjectManagerService pmService;
	private int deptId;
	private int empId;
	private String empName;
	private int tempId;
	private int toolTotal;
	private int fixedTotal;
	private int itTotal;
	private int memberTotal;
	private int projectTotal;
	
	public AdminController(AdminService theprojectService,ProjectManagerService pmService) {
		adminService=theprojectService;
		this.pmService = pmService;
	}	
	
	//profile
	@GetMapping("/adminprofile")
	public String profile(Model model) {
		Employee profile = pmService.findById(empId);
		model.addAttribute("Emp_Name",empName);
		model.addAttribute("profile", profile);
		
		return "admin/profile";
	}
	
	@PostMapping("/adminchangepassword")
	public String ChangePass(@RequestParam("current") String currentpass,@RequestParam("password1")String newpass,@RequestParam("password2")String confirmPass,Model model) {
		Employee profile = pmService.findById(empId);
		error="";
		model.addAttribute("profile", profile);
		String pass=profile.getPassword();
		if(pass.equals(currentpass) && confirmPass.equals(currentpass)) {
			profile.setPassword(newpass);
			pmService.saveProfile(profile);
			return "redirect:/admin/adminprofile";
		}
		else {
			error="Something wrong !";
			
			return "redirect:/admin/adminprofile";
		}
		
	}
	
//Department (Start)	
	@GetMapping("/adminViewDepartment")
	public String AdminViewDepart(Model model) {
		try {
			tempId = (int)model.asMap().get("employeeID");	
			if(tempId == 0)
			{
				tempId = empId;
			}
			else {
				empId = tempId;
			}
		}
		catch(Exception e) {
			System.out.println("Temp Employee ID is null");
			
		}	
		Employee profile=pmService.findById(empId);
		empName = profile.getEmp_name();
		model.addAttribute("empName",empName);
		
		List<Department> deptList=adminService.findAllDept();
		//model.addAttribute("deptName",DeptmemberList.get(0).getDepartmentEmp().getDept_name());
		
		
		model.addAttribute("dept_card",deptList);
		return "admin/adminViewDepartment";
		
	}
	@GetMapping("/adminViewProject")
	public String admin(  Model model) {
		List<Project> projectList=adminService.findAll();
		model.addAttribute("admin_view_project",projectList);
		model.addAttribute("empName",empName);
		return "admin/adminViewProject";
	}
	

	@GetMapping("/addDeptForm")
	public String adminAddDeptModel(Model model) {
		Department dept = new Department();
		model.addAttribute("addDept", dept);
		model.addAttribute("empName",empName);
		return "admin/addDepartment";
	}
	
	@PostMapping("/add_dept")
	
	public String saveNewDept(@ModelAttribute("addDept") Department theDept) {
		Department dept = new Department();
		adminService.addNewDept(theDept);
		return "redirect:/admin/AdminViewProject";		
	}	
	
	@GetMapping("/adminViewDeptMember")
	public String adminViewDeptMember(@RequestParam("Dept_id") int dept_id,Model model) {
		deptId = dept_id;
		List<Employee> DeptmemberList = adminService.DeptMemberList(dept_id);
		model.addAttribute("dept_member",DeptmemberList);
		model.addAttribute("deptName",DeptmemberList.get(0).getDepartmentEmp().getDept_name());
		//System.out.println("Emp : " + DeptmemberList.get(0).getEmp_id());
		model.addAttribute("empName",empName);
		model.addAttribute("dept_id",deptId);
		return "admin/adminViewDeptMember";
	}
	
	@GetMapping("/adminDeptFixAsset")
	public String adminViewDeptFixAsset(Model model) {
		List<DeptFixedAsset> DeptFixAsset = adminService.DeptFixedAsset(deptId);
		 fixedTotal=DeptFixAsset.size();
			model.addAttribute("deptfixedTotal","Total "+fixedTotal);
			model.addAttribute("deptitTotal","Total "+itTotal);
		model.addAttribute("dept_fix_asset",DeptFixAsset);
		model.addAttribute("empName",empName);
		model.addAttribute("dept_id",deptId);
		return "admin/adminDeptFixedAsset";
	}
	
	@GetMapping("/adminDeptProject")
	public String adminViewDeptProject( Model model) {
		List<DeptProject> DeptProject = adminService.DeptProject(deptId);
		model.addAttribute("dept_project",DeptProject);
		//System.out.println("Emp : " + DeptmemberList.get(0).getEmp_id());
		model.addAttribute("empName",empName);
		model.addAttribute("dept_id",deptId);
		return "admin/adminDeptProject";
	}
	
	@GetMapping("/adminViewDeptItAsset")
	public String adminViewDeptITAsset( Model model) {
		List<DeptItAsset> DeptItAsset = adminService.DeptItAsset(deptId);
		itTotal=DeptItAsset.size();
		model.addAttribute("deptfixedTotal","Total "+fixedTotal);
		model.addAttribute("deptitTotal","Total "+itTotal);
		model.addAttribute("it_asset",DeptItAsset);		//System.out.println("Emp : " + DeptmemberList.get(0).getEmp_id());
		model.addAttribute("empName",empName);
		model.addAttribute("dept_id",deptId);
		return "admin/adminDeptItAsset";
	}		
	
	@GetMapping("/DeptprojectMember")
	public String DeptprojectMember(@RequestParam("pj_id") int pj_id ,Model model) {
		System.out.println("Ha Ha ");
		List<ProjectAssign> memberList = adminService.showMemberList(pj_id);
		model.addAttribute("memberList",memberList);
		//System.out.println("Emp : " + memberList.get(0).getEmp_id());
		model.addAttribute("empName",empName);
		model.addAttribute("dept_id",deptId);
		return "admin/DeptProjectMember";
	}
	
	//Department(end)
		
	//Project(start)
	@GetMapping("/projectMember")
	public String adminViewProjectMember(@RequestParam("pj_id") int pj_id ,Model model) {
		//System.out.println("Ha Ha ");
		List<ProjectAssign> memberList = adminService.showMemberList(pj_id);
		model.addAttribute("memberList",memberList);
		//System.out.println("Emp : " + memberList.get(0).getEmp_id());
		model.addAttribute("empName",empName);
		return "admin/adminViewProjectMember";
	}
	//Project(end)
	
	
	//Employee(start)
	@GetMapping("/formForAddEmployee")
	public String formForAddEmployee(Model model) {
		Employee employee = new Employee();
		List<Department> dept=adminService.findAllDept();
		model.addAttribute("department", dept);
		model.addAttribute("employee", employee);
		model.addAttribute("empName",empName);
		return "admin/adminAddNewEmployee";
	}
	@GetMapping("leaveEmployee")
	public String leaveEmployee(@RequestParam("emp_id") int emp_id,Model model) {
		Employee emp=adminService.findById(emp_id);
		Date end_date = new java.sql.Date(new java.util.Date().getTime());
		emp.setEnd_date(end_date);
		adminService.addEmployee(emp);
		return "redirect:/admin/adminViewEmployee";
	}
	
	
	@PostMapping("/addEmployee")
	public String addEmployee(@ModelAttribute("employee") Employee theEmployee) {
		theEmployee.setAvailable(true);
		adminService.addEmployee(theEmployee);		
		return "redirect:/admin/adminViewEmployee";
		
	}
	
	@GetMapping("/formForUpdateEmployee")
	public String formForUpdateEmployee(@RequestParam("emp_id") int emp_id,Model model) {
		Employee employee = adminService.findById(emp_id);
		List<Department> dept=adminService.findAllDept();
		model.addAttribute("department", dept);
		model.addAttribute("employee", employee);
		model.addAttribute("empName",empName);
		return "admin/adminUpdateEmployee";
		
	}
	
	@GetMapping("/adminViewEmployee")
	public String listEmployee(Model model) {
		List<Employee> list = adminService.findAllEmployee();
		List<Department> dept=adminService.findAllDept();
		model.addAttribute("employee", list);
		model.addAttribute("department",dept);
		model.addAttribute("empName",empName);
		return "admin/adminViewEmployee";
	}

	//Employee(end)
	
	//Asset (Start)
	/*
	 * write admin's fixed asset*/
	@GetMapping("/adminFixedAsset")
	public String showFixedAsset(Model model) {
		List<FixedAsset> fixedlist = adminService. findAllFixedAsset();
		List<ItAsset> ITlist = adminService.findAllItAsset();
		List<ToolAsset> Toollist = adminService.findAllToolAsset();
		 fixedTotal=fixedlist.size();
		 itTotal=ITlist.size();
		 toolTotal=Toollist.size();
		 model.addAttribute("total","Total "+toolTotal);
		model.addAttribute("fixedTotal","Total "+fixedTotal);
		model.addAttribute("itTotal","Total "+itTotal);
		model.addAttribute("admin", fixedlist);
		model.addAttribute("empName",empName);
		//System.out.print("fixed_asset_id"+fixedlist.get(0).getFixed_asset_id());
		return "admin/AdminViewFixedAssetList";
	}
	/*
	 *  write admin's IT asset*/
	@GetMapping("/adminItAsset")
	public String showItAsset(Model model) {
		List<ItAsset> itlist = adminService.findAllItAsset();
		 itTotal=itlist.size();
		model.addAttribute("total","Total "+toolTotal);
		model.addAttribute("fixedTotal","Total "+fixedTotal);
		model.addAttribute("itTotal","Total "+itTotal);
		model.addAttribute("admin", itlist);
		model.addAttribute("empName",empName);
		//System.out.print("it_asset_id"+itlist.get(0).getIt_asset_id());
		return "admin/AdminViewITAssetList";
	}
	/*
	 *  write admin's Tool asset*/
	@GetMapping("/adminToolAsset")
	public String showToolAsset(Model model) {
		List<ToolAsset> toollist = adminService.findAllToolAsset();
		 toolTotal=toollist.size();
		model.addAttribute("total","Total "+toolTotal);
		model.addAttribute("fixedTotal","Total "+fixedTotal);
		model.addAttribute("itTotal","Total "+itTotal);
		model.addAttribute("admin", toollist);
		model.addAttribute("empName",empName);
		//System.out.print("tool_asset_id"+toollist.get(0).getTool_asset_id());
		return "admin/AdminViewToolAssetList";
	}
	//Asset (End)

}
