package com.teamA.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.service.AdminService;
import com.teamA.demo.service.DepartmentHeadService;
import com.teamA.demo.service.MemberService;
import com.teamA.demo.service.ProjectManagerService;

@Controller
@RequestMapping("/departmenthead")
public class DepartmentHeadController {
	
	private DepartmentHeadService departmentheadService;
	private ProjectManagerService pmService;
	private AdminService adminService;
	private int Emp_Id;
	private int temp_id;
	private String Emp_Name;
	private int deptID;
	private int pjId;
	private int toolTotal;
	private int fixedTotal;
	private int itTotal;
	private String historyId;
	public String error="";
	
	
	public DepartmentHeadController(DepartmentHeadService departmentHeadService,ProjectManagerService pmService,AdminService adminService) {
		departmentheadService = departmentHeadService;
		this.pmService = pmService;
		this.adminService = adminService;
	}
	
	@GetMapping("/projectlists")
	public String Dashboard(Model model) {
		try {
			temp_id = (int)model.asMap().get("employeeID");	
			if(temp_id == 0)
			{
				temp_id = Emp_Id;
			}
			else {
				Emp_Id = temp_id;
			}
		}
		catch(Exception e) {
			System.out.println("Temp Employee ID is null");
		}	
		Employee profile=pmService.findById(Emp_Id);
		Emp_Name = profile.getEmp_name();
		model.addAttribute("Emp_Name",Emp_Name);
		deptID = profile.getDept_id();
		List<DeptProject> list = departmentheadService.findProjectForEachDepartment(deptID);
		model.addAttribute("pjLists", list);
		return "departmenthead/projectlists";
	}
	
	@GetMapping("/pending")
	public String pending(Model model) {
		List<Requests> list=departmentheadService.findPendingProject(deptID);
		model.addAttribute("pendingpj",list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/pending";
	}
	@GetMapping("/pendingViewpage")
	public String pendingView(@RequestParam("ToolSetId")String toolSetId,Model model) {
		historyId=toolSetId;
		List<Requests> list=departmentheadService.ReuestToolList(historyId);
		model. addAttribute("pendingViewLists",list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/pendingView";
	}
	@GetMapping("/acceptToolHistory")
	public String AcceptHistory(@RequestParam("requestId")int request_id) {
		departmentheadService.AcceptHistoryTool(request_id);
		return "redirect:/departmenthead/pendingViewpage?ToolSetId=" + historyId;
	}
	
	@GetMapping("/rejectToolHistory")
	public String RejecttHistory(@RequestParam("requestId")int request_id) {
		departmentheadService.RejectHistoryTool(request_id);
		return "redirect:/departmenthead/pendingViewpage?ToolSetId=" + historyId;
	}
	@GetMapping("/accept")
	public String accept(Model model) {
		List<Requests> list=departmentheadService.findAccept(deptID);
		model.addAttribute("acceptpj", list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/accept";
	}
	@GetMapping("/acceptViewPage")
	public String acceptView(@RequestParam("ProjectId")int pj_id,Model model) {
		List<Requests> list=departmentheadService.AcceptViewTools(pj_id);
		model.addAttribute("accview",list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/acceptView";
	}
	@GetMapping("/reject")
	public String reject(Model model) {
		List<Requests> list=departmentheadService.findReject(deptID);
		model.addAttribute("rejectProject",list);
		model.addAttribute("Emp_Name",Emp_Name);
 		return "departmenthead/reject";
	}
	@GetMapping("/rejectViewPage")
	public String rejectView(@RequestParam("ProjectId")int pj_id,Model model) {
		List<Requests> list=departmentheadService.RejectViewTools(pj_id);
		model.addAttribute("rejectview",list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/rejectView";
	}
	
	@GetMapping("/employeelists")
	public String employeeLists(Model model) {
		List<Employee> list = departmentheadService.findEmployeeList(deptID);
		model.addAttribute("listemp", list);
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/employeelists";
	}
	@PostMapping("/changePassword")
	public String ChangePass(@RequestParam("current") String currentpass,@RequestParam("password1")String newpass,@RequestParam("password2")String confirm,Model model) {
		Employee profile = pmService.findById(Emp_Id);
		error="";
		model.addAttribute("profile", profile);
		String pass=profile.getPassword();
		if(pass.equals(currentpass) && confirm.equals(currentpass)) {
			profile.setPassword(newpass);
			pmService.saveProfile(profile);
			return "redirect:/departmenthead/deptProfile";
		}
		else {
			error="Something wrong!";
			return "redirect:/departmenthead/deptProfile";
		}
		
	}
	@GetMapping("/deptProfile")
	public String profile(Model model) {
		Employee profile = pmService.findById(Emp_Id);
		model.addAttribute("Emp_Name",Emp_Name);
		model.addAttribute("profile", profile);
		model.addAttribute("error",error);
		return "departmenthead/profile";
	}
	
	
		@GetMapping("/fixedAssetList")
		public String showFixedAsset(Model model) {
			List<DeptFixedAsset> assetList=departmentheadService.findFixedAsset(deptID);
			List<DeptItAsset> assetITList = departmentheadService.findAllItAsset(deptID);
			model.addAttribute("deptAsset", assetITList);
			itTotal=assetITList.size();
			model.addAttribute("deptFixedAsset",assetList);
			fixedTotal=assetList.size();
			model.addAttribute("toolTotal","Total "+toolTotal);
			model.addAttribute("fixedTotal","Total "+fixedTotal);
			model.addAttribute("itTotal","Total "+itTotal);
			model.addAttribute("Emp_Name",Emp_Name);
			return "departmenthead/fixedAssetList";
		}
		
		@GetMapping("/itAssetList")
		public String showItAsset(Model model) {
			List<DeptItAsset> assetITList = departmentheadService.findAllItAsset(deptID);
			model.addAttribute("deptAsset", assetITList);
			itTotal=assetITList.size();
			model.addAttribute("toolTotal","Total "+toolTotal);
			model.addAttribute("fixedTotal","Total "+fixedTotal);
			model.addAttribute("itTotal","Total "+itTotal);
			model.addAttribute("Emp_Name",Emp_Name);
			return "departmenthead/iTAssetList";
		}
		
		
	@GetMapping("/memberList")
	public String showMemberList(@RequestParam("pj_id") int pj_id, Model model) {
		List<ProjectAssign> memberList = adminService.showMemberList(pj_id);
		model.addAttribute("memberList",memberList);
		model.addAttribute("projectName", memberList.get(0).getProjectEmp().getPj_name());
		model.addAttribute("Emp_Name",Emp_Name);
		return "departmenthead/memberView";
	}
	
	
	


}
