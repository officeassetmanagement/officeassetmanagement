package com.teamA.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.service.DepartmentHeadService;
import com.teamA.demo.service.MainService;
import com.teamA.demo.service.MemberService;
import com.teamA.demo.service.ProjectManagerService;

@Controller
@RequestMapping("/main")

public class MainController {
	private ProjectManagerService pmService;
	private MemberService memberService;
	private MainService mainService;
	private ProjectManagerController pmController;
	private int EMP_ID;
	private DepartmentHeadService depthService;
	public String error="";
	
	public MainController(ProjectManagerService thepmService,MemberService mService,MainService maService,ProjectManagerController mController,DepartmentHeadService dhService) {
		pmService=thepmService;
		memberService = mService;
		mainService=maService;
		pmController=mController;
		depthService=dhService;
	}
		
	@GetMapping("/login")
	public String login(Model model) {
		model.addAttribute("error", error);
		return "main/login";
	}
	
	@PostMapping("/which_role")
	public String which_role(@RequestParam("email") String email,@RequestParam("password") String password,Employee employee,  Model model,RedirectAttributes redirectAttributes) {
		List<Employee> user=mainService.findUser();
		error="";
		for(int i=0;i<user.size();i++) {
			String emp=user.get(i).getRole_name();
			String email1=user.get(i).getEmail();
			String pass=user.get(i).getPassword();
					
			if(email1.equals(email) && pass.equals(password)){
				EMP_ID = user.get(i).getEmp_id();
				redirectAttributes.addFlashAttribute("employeeID", EMP_ID);
				if(emp.equals("Admin")) {					
					return "redirect:/admin/adminViewDepartment";
				}
				else if(emp.equals("Operation Manager")) {					
					return "redirect:/OperationTeam/fixedasset";
				}
				else if(emp.equals("Department Head")) {
					return "redirect:/departmenthead/projectlists";
				}
				else if(emp.equals("Project Manager")) {
					return "redirect:/project_manager/pm_dashboard";
				}
				else if(emp.equals("Member")) {
					return "redirect:/member/project";
				}								
				
			}
	
		}
		error="Something wrong ! Try Again!";
		return "redirect:/main/login";
		
	}
}
