package com.teamA.demo.controller;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.service.MemberService;
import com.teamA.demo.service.ProjectManagerService;

@Controller
@RequestMapping("/member")
public class MemberController { 
	
	private MemberService memberService;
	private ProjectManagerService pmService;
	private int EMP_ID; 
	private int temp_id;
	private String Emp_Name = "";
	private int Pj_ID;
	private String tsId = "ts1";
	private Date current_date;
	public String error="";
		
	public MemberController(MemberService mService,ProjectManagerService pmService) {		
		memberService = mService;
		this.pmService = pmService;
	}
	
	@GetMapping("/project_detail")
	public String member(@RequestParam("pj_id") int pj, Model model) {
		current_date = new Date();
		Pj_ID = pj;
		Project projectDetail = memberService.showProjectDetail(pj);
		model.addAttribute("projectDetail",projectDetail);		
		Employee pm = pmService.findById(projectDetail.getEmp_id());
		model.addAttribute("projectManager", pm.getEmp_name());
			
		List<ToolAsset> toolList = memberService.showToolAssetList();
		model.addAttribute("ToolAsset", toolList);
		
		List<Requests> requests = memberService.showRequests(pj);
		model.addAttribute("Requests",requests);
		
		List<Requests> acceptProject = memberService.showAcceptProject(pj);
		model.addAttribute("AcceptProject",acceptProject);
		
		List<Requests> rejectProject = memberService.showRejectProject(pj);
		model.addAttribute("RejectProject",rejectProject);
		model.addAttribute("Emp_Name", Emp_Name);
		
		Requests request = new Requests();
		model.addAttribute("request",request);
		
		List<Requests> currentRequests = memberService.showRequestTools(tsId);
		model.addAttribute("currentRequests", currentRequests);
		
		return "member/project_detail";
	}
	
	@GetMapping("/project")
	public String memberProject(Model model) {		
		try {
			temp_id = (int)model.asMap().get("employeeID");	
			if(temp_id == 0)
			{
				temp_id = EMP_ID;
			}
			else {
				EMP_ID = temp_id;
			}
		}
		catch(Exception e) {
			System.out.println("Temp Employee ID is null");
		}			
		System.out.println("Employee ID ::: " + EMP_ID);
		List<ProjectAssign> projectList = memberService.showProjectForMember(EMP_ID);
		model.addAttribute("projectList",projectList);		
		Employee profile=pmService.findById(EMP_ID);
		Emp_Name = profile.getEmp_name();
		model.addAttribute("profile",profile);	
		
		
		
		List<Requests> allReq = memberService.showAllRequest();
		if(allReq.size() >= 1) {
			tsId = allReq.get(allReq.size()-1).getTool_set_id();
			int id = Integer.parseInt(tsId.substring(2, tsId.length()));
			id++;
			tsId = tsId.substring(0, 2) + id;
		}
		else {
			tsId = "ts1";
		}
		return "member/project";
	}	
	@GetMapping("/removeRequestTool")
	public String RemoveRequestTool(@RequestParam("RequestId")int reqId) {
		memberService.removeById(reqId);
		return "redirect:/member/project_detail?pj_id="+ Pj_ID;
	}
	
	@GetMapping("/pending")//(member requested asset but not accept)
	public String pending( Model model) { 
		System.out.println("Employee ID ::: " + EMP_ID);
		List<Requests> pending = memberService.showPending(EMP_ID);
		model.addAttribute("Pending",pending);
		model.addAttribute("Emp_Name", Emp_Name);
		return "member/pending";
	}
	
	@GetMapping("/accept")//(member requested asset accepted by operation manager)
	public String accept(Model model) {
		List<Requests> accept = memberService.showAccept(EMP_ID);
		model.addAttribute("AcceptList", accept);
		model.addAttribute("Emp_Name", Emp_Name);
		return "member/accept";
	}
	
	@GetMapping("/reject")//(member requested asset rejected by operation manager)
	public String reject(Model model) {
		List<Requests> reject = memberService.showReject(EMP_ID); 
		model.addAttribute("RejectList", reject);
		model.addAttribute("Emp_Name", Emp_Name);
		return "member/reject";
	}
	
	@GetMapping("/profile")
	public String profile(Model model) {
		Employee profile = pmService.findById(EMP_ID);
		model.addAttribute("profile", profile);
		model.addAttribute("error", error);
		return "member/profile";
	}
	
	@PostMapping("/updateProfile")
	public String UpdateProfile(@ModelAttribute("profile") Employee employee) {
		pmService.saveProfile(employee);
		return "redirect:/member/project";
	}
	
	@PostMapping("/changePassword")
	public String ChangePass(@RequestParam("current") String currentpass,@RequestParam("password2")String confirm,@RequestParam("password1")String newpass,Model model) {
		Employee profile = pmService.findById(EMP_ID);
		model.addAttribute("profile", profile);
		error="";
		String pass=profile.getPassword();
		if(pass.equals(currentpass) && confirm.equals(newpass)) {
			profile.setPassword(newpass);
			pmService.saveProfile(profile);
			return "redirect:/member/profile";
		}
		else {
			error="Something wrong !";
			return "redirect:/member/profile";
		}
		
	}
	
	@PostMapping("/addRequest")
	public String addRequest(@ModelAttribute("request") Requests request, Model model){
		//Requests tool=memberService.getRequestTool("T-A-0001", 8);
		
		request.setTool_set_id(tsId);
		request.setEmp_id(EMP_ID);
		request.setPj_id(Pj_ID);
		request.setAcceptby("");
		request.setRejectby("");
		request.setDate_time(current_date);
		request.setAccept_qty(0);
	    memberService.saveRequest(request);	    
		return "redirect:/member/project_detail?pj_id="+ Pj_ID;
		
	}
	
	@GetMapping("/confirmRequest")
	public String confirmRequest(Model model) {
		int id = Integer.parseInt(tsId.substring(2, tsId.length()));
		id++;
		tsId = tsId.substring(0, 2) + id;
		return "redirect:/member/project_detail?pj_id="+ Pj_ID;
	}
	@GetMapping("/cancelRequest")
	public String CancelRequest(Model model) {
		memberService.removeRequest(tsId);
		return "redirect:/member/project";
	}

}
