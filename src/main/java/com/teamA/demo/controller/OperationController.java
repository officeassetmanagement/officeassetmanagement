package com.teamA.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamA.demo.service.AdminService;
import com.teamA.demo.service.DepartmentHeadService;
import com.teamA.demo.service.MemberService;
import com.teamA.demo.service.OperationService;
import com.teamA.demo.service.ProjectManagerService;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.EmployeeAsset;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.ToolAssetDamage;

@Controller
@RequestMapping("/OperationTeam")
public class OperationController {

	private OperationService opService;
	private AdminService adminService;
	private ProjectManagerService pmService;
	private DepartmentHeadService departmentheadService;
	private MemberService memberService;
	private String tool_set_id;
	private String tsid;
	private String tool_asset_id;
	private int toolTotal;
	private int fixedTotal;
	private int itTotal;
	private int emp_id;
	private int Emp_Id;
	private int temp_id;
	private String Emp_Name = "";
	private int deptId;
	private int userid;
	private int pjid;
	public String error="";
	// Constructor
	public OperationController(OperationService operationService, AdminService adminService,
			ProjectManagerService pmService, DepartmentHeadService departmentheadService, MemberService mService) {
		opService = operationService;
		this.adminService = adminService;
		this.pmService = pmService;
		this.departmentheadService = departmentheadService;
		this.memberService = mService;
	}
	
	private void endEmployeeWorkLife() {
		List<Employee> allEmp = adminService.findAllEmployee();
		for(int i=0; i<allEmp.size(); i++) {
			if(allEmp.get(i).getEnd_date()!=null) {
				List<EmployeeAsset> empAsset = opService.showUserDevices(allEmp.get(i).getEmp_id());
				if(empAsset.size() >= 1) {
					for(int j=0; j<empAsset.size(); j++) {
						ItAsset itAsset = opService.findByITId(empAsset.get(j).getIt_asset_id());
						itAsset.setAvailable(true);
						opService.save(itAsset);
						//opService.removeUserDevice(empAsset.get(j).getId());
						opService.removeDeptItAsset(empAsset.get(j).getEmployeeAsset().getDept_id(),empAsset.get(j).getIt_asset_id());
					}
				}
			}
		}
	}


	@GetMapping("/fixedasset")
	public String FixedAsset(Model model) {
		endEmployeeWorkLife(); //initial checking for leaved employee
		try {
			temp_id = (int) model.asMap().get("employeeID");
			if (temp_id == 0) {
				temp_id = Emp_Id;
			} else {
				Emp_Id = temp_id;
			}
		} catch (Exception e) {
			System.out.println("Temp Employee ID is null");
		}
		Employee profile = pmService.findById(Emp_Id);
		Emp_Name = profile.getEmp_name();
		model.addAttribute("Emp_Name", Emp_Name);

		List<FixedAsset> assetList = opService.findAll();
		fixedTotal = assetList.size();
		model.addAttribute("assetList", assetList);
		model.addAttribute("total", "Total_" + opService.findAllTool().size());
		model.addAttribute("fixedTotal", "Total_" + opService.findAll().size());
		model.addAttribute("itTotal", "Total_" + opService.findAllIT().size());
		List<Department> deptList = adminService.findAllDept();
		model.addAttribute("deptList",deptList);
		DeptFixedAsset deptFixedAsset = new DeptFixedAsset();
		model.addAttribute("deptFixedAsset",deptFixedAsset);
		return "OperationTeam/showFixedAsset";
	}
	
	@GetMapping("/projects")
	public String projects(Model model) {
		model.addAttribute("Emp_Name", Emp_Name);
		List<Project> pjList = adminService.findAll();
		model.addAttribute("projectList",pjList);
		return "OperationTeam/projects";
	}
	
	@GetMapping("/history")
	public String history(Model model) {
		model.addAttribute("Emp_Name", Emp_Name);
		List<Requests> history = opService.showHistory();
		model.addAttribute("history",history);
		return "OperationTeam/history";
	}
	
	@GetMapping("/viewToolRequests")
	public String viewToolRequests(@RequestParam("toolId")String tool_asset_id, Model model) {
		model.addAttribute("Emp_Name", Emp_Name);
		List<Requests> reqList = opService.showRequestsByToolId(tool_asset_id);
		model.addAttribute("reqList",reqList);
		return "OperationTeam/showToolRequests";
	}
	
	@GetMapping("/profile")
	public String Profile(Model model) {
		
		Employee profile = pmService.findById(Emp_Id);
		model.addAttribute("profile", profile);
		model.addAttribute("error", error);
		return "OperationTeam/profile";
	}
	
	@PostMapping("/changePassword")
	public String ChangePass(@RequestParam("current") String currentpass,@RequestParam("password1")String newpass,@RequestParam("password2")String confirm,Model model) {
		Employee profile = pmService.findById(Emp_Id);
		
		model.addAttribute("profile", profile);
		String pass=profile.getPassword();
		if(pass.equals(currentpass) && newpass.equals(confirm)) {
			profile.setPassword(newpass);
			pmService.saveProfile(profile);
			return "redirect:/OperationTeam/profile";
		}
		else {
			error="Something wrong !";
			return "redirect:/OperationTeam/profile";
		}
		
	}
	
	@GetMapping("/showUsers")
	public String ShowUsers(Model model) {
		List<Employee> userList=opService.getAllEmployee();
		model.addAttribute("userList", userList);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showUsers";
	}
	@GetMapping("/showDevices")
	public String ShowDevice(@RequestParam("empId")int empId,Model model) {
		//List<ItAsset> deviceList = new ArrayList<ItAsset>();
		List<ItAsset> itAssetList=opService.getDevices();
		//List<EmployeeAsset> usedDeviceList = opService.getUsedDevices();
		/*
		 * for(int i=0; i<itAssetList.size(); i++) { boolean flag = false; for(int j=0;
		 * j<usedDeviceList.size(); j++) {
		 * if(itAssetList.get(i).getIt_asset_id().equals(usedDeviceList.get(j).
		 * getIt_asset_id())) { flag = true; } } if(!flag) {
		 * deviceList.add(itAssetList.get(i)); } }
		 */
		model.addAttribute("itAssetList", itAssetList);
		userid = empId;
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/devices";
	}
	
	@GetMapping("/viewUser")
	public String viewUser(@RequestParam("itId")String it_asset_id, Model model) {
		List<EmployeeAsset> userList = opService.showUsers(it_asset_id);
		model.addAttribute("Emp_Name", Emp_Name);
		model.addAttribute("userList",userList);
		return "OperationTeam/viewUsers";
	}
	
	@GetMapping("/showUserDevices")
	public String showUserDevices(@RequestParam("empId")int empId,Model model) {
		userid=empId;
		model.addAttribute("empid",empId);
		model.addAttribute("Emp_Name", Emp_Name);
		List<EmployeeAsset> userDevices = opService.showUserDevices(empId);
		model.addAttribute("userDevices",userDevices);
		model.addAttribute("userName",adminService.findById(empId).getEmp_name());
		model.addAttribute("id",adminService.findById(empId).getEmp_id());
		return "OperationTeam/showUserDevices";
	}
	
	@GetMapping("/removeUserDevice")
	public String removeUserDevice(@RequestParam("id")int id, Model model) {
		EmployeeAsset empAsset = opService.getEmployeeAssetById(id);
		ItAsset itAsset = opService.findByITId(empAsset.getIt_asset_id());
		itAsset.setAvailable(true);
		opService.save(itAsset);
		opService.removeUserDevice(id);
		opService.removeDeptItAsset(empAsset.getEmployeeAsset().getDept_id(),empAsset.getIt_asset_id());
		return "redirect:/OperationTeam/showUserDevices?empId="+userid;
	}
	
	@GetMapping("/addUserDevice")
	public String addUserDevice(@RequestParam("itid")String it_asset_id, Model model) {
		EmployeeAsset empAsset = new EmployeeAsset(userid,it_asset_id,new Date());
		opService.saveUserDevice(empAsset);
		ItAsset itAsset = opService.findByITId(it_asset_id);
		itAsset.setAvailable(false);
		Employee emp = adminService.findById(userid);
		DeptItAsset deptItAsset = new DeptItAsset(emp.getDept_id(),it_asset_id);
		opService.saveDeptItAsset(deptItAsset);
		return "redirect:/OperationTeam/showUserDevices?empId="+userid;
	}
	
	

	@GetMapping("/itasset")
	public String ITAsset(Model model) {
		List<ItAsset> assetITList = opService.findAllIT();
		itTotal = assetITList.size();
		model.addAttribute("assetITList", assetITList);
		model.addAttribute("total", "Total_" + toolTotal);
		model.addAttribute("fixedTotal", "Total_" + fixedTotal);
		model.addAttribute("itTotal", "Total_" + itTotal);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showITAsset";
	}
	
	@GetMapping("/toolasset")
	public String ToolAsset(Model model) {
		List<ToolAsset> toolList = opService.findAllTool();
		toolTotal = toolList.size();
		model.addAttribute("toolList", toolList);
		model.addAttribute("total", "Total_" + toolTotal);
		model.addAttribute("fixedTotal", "Total_" + fixedTotal);
		model.addAttribute("itTotal", "Total_" + itTotal);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showToolAsset";
	}

	@GetMapping("/updateFixed")
	public String updateFixed(@RequestParam("fixedId") String fixed_asset_id, Model model) {
		FixedAsset fixedAset = opService.findById(fixed_asset_id);
		model.addAttribute("fixedAsset", fixedAset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insert";
	}

	@GetMapping("/insertFixed")
	public String insertFixed(Model model) {
		FixedAsset fixedAsset = new FixedAsset();
		model.addAttribute("fixedAsset", fixedAsset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insert";
	}

	@PostMapping("/save")
	public String saveFixed(@ModelAttribute("fixedAsset") FixedAsset fixedAsset) {
		fixedAsset.setAvailable(true);
		opService.save(fixedAsset);
		return "redirect:/OperationTeam/fixedasset";
	}

	@GetMapping("/delete")
	public String deleteEmployee(@RequestParam("fixedId") String fixed_asset_id) {
		opService.deleteById(fixed_asset_id);
		return "redirect:/OperationTeam/fixedasset";
	}

	@GetMapping("/insertIT")
	public String insertIt(Model model) {
		ItAsset itAsset = new ItAsset();
		model.addAttribute("itAsset", itAsset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insertItAsset";
	}

	@PostMapping("/saveITAsset")
	public String saveITAsset(@ModelAttribute("itAsset") ItAsset itAsset) {
		itAsset.setAvailable(true);
		opService.save(itAsset);
		return "redirect:/OperationTeam/itasset";
	}

	@GetMapping("/updateIT")
	public String updateIT(@RequestParam("itId") String it_asset_id, Model model) {
		ItAsset itAsset = opService.findByITId(it_asset_id);
		model.addAttribute("itAsset", itAsset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insertItAsset";
	}

	@GetMapping("/deleteIT")
	public String deleteITAsset(@RequestParam("itId") String it_asset_id) {
		opService.deleteByITId(it_asset_id);
		return "redirect:/OperationTeam/itasset";
	}

	// insert form
	@GetMapping("/insertTool")
	public String insertTool(Model model) {
		ToolAsset toolAsset = new ToolAsset();
		model.addAttribute("toolAsset", toolAsset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insertToolAsset";
	}

	// insert tool asset
	@PostMapping("/saveToolAsset")
	public String saveTool(@ModelAttribute("toolAsset") ToolAsset toolAsset) {
		toolAsset.setAvailable_qty(toolAsset.getQty());
		opService.save(toolAsset);
		return "redirect:/OperationTeam/toolasset";

	}

	@GetMapping("/updateTool")
	public String updateTool(@RequestParam("toolId") String tool_asset_id, Model model) {
		ToolAsset toolAset = opService.findToolById(tool_asset_id);
		model.addAttribute("toolAsset", toolAset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/insertToolAsset";
	}

	@GetMapping("/deleteTool")
	public String deleteToolAsset(@RequestParam("toolId") String tool_asset_id) {
		opService.deleteToolById(tool_asset_id);
		return "redirect:/OperationTeam/toolasset";
	}

	@GetMapping("/showDamage")
	public String showDamage(Model model) {
		List<ToolAssetDamage> damageList = opService.getDamageTools();
		model.addAttribute("damageList", damageList);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showDamage";
	}

	@GetMapping("/showPending")
	public String showPending(Model model) {
		List<Requests> pending = opService.getRequest();
		model.addAttribute("Pending", pending);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showPending";
	}

	@GetMapping("/pendingView")
	public String pendingView(@RequestParam("tool_set_id") String ts_id, Model model) {
		tsid = ts_id;
		List<Requests> pending = opService.pendingView(ts_id);
		model.addAttribute("Pending", pending);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/pendingView";
	}

	@GetMapping("/acceptTool")
	public String AcceptTool(@RequestParam("request_id")int req_id,Model model) {
		Requests req = opService.getRequestById(req_id);
		int accept_qty;
		int reject_qty;
		int available_qty;
		if(req.getToolAssetRequests().getAvailable_qty() == 0) {
			reject_qty = req.getRequest_qty();
			available_qty = 0;
		}
		if(req.getToolAssetRequests().getAvailable_qty() < req.getRequest_qty()) {
			accept_qty = req.getToolAssetRequests().getAvailable_qty();
			reject_qty = req.getRequest_qty() - req.getToolAssetRequests().getAvailable_qty();
			available_qty = 0;
			req.setReject_qty(reject_qty);
			req.setRejectby("Operation Manager");
		}
		else {
			accept_qty = req.getRequest_qty();
			available_qty = req.getToolAssetRequests().getAvailable_qty() - accept_qty;
		}
		//opService.acceptById(req_id); this method is not needed to update
		req.setAccept_qty(accept_qty);
		//req.setReject_qty(reject_qty); this is new method to add
		req.setAcceptby("Operation Manager");
		memberService.saveRequest(req);
		ToolAsset asset = opService.findToolById(req.getTool_asset_id());
		asset.setAvailable_qty(available_qty);
		opService.save(asset);
		model.addAttribute("Emp_Name", Emp_Name);
		return "redirect:/OperationTeam/pendingView?tool_set_id="+req.getTool_set_id();
	}
	@GetMapping("/rejectTool")
	public String RejectTool(@RequestParam("request_id")int req_id,Model model) {
		opService.rejectById(req_id);
		model.addAttribute("Emp_Name", Emp_Name);
		return "redirect:/OperationTeam/pendingView?tool_set_id="+tsid;
	}

	@GetMapping("/showAcceptList")
	public String accept(Model model) {
		List<Requests> list = opService.findAccept("Operation Manager");
		model.addAttribute("acceptpj", list);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showAcceptList";
	}

	@GetMapping("/acceptToolList")
	public String acceptToolList(@RequestParam("pj_id") int pj_id, Model model) {
		pjid = pj_id;
		List<Requests> accept = opService.acceptView(pj_id);
		int toolSize = accept.size();
		model.addAttribute("accept", accept);
		model.addAttribute("toolSize", toolSize);
		model.addAttribute("Emp_Name", Emp_Name);
		/*
		 * ToolAssetDamage damage = new ToolAssetDamage();
		 * model.addAttribute("toolDamage",damage);
		 * model.addAttribute("tool_set_id",ts_id);
		 */
		model.addAttribute("taken",accept.get(0).getOut_date());
		return "OperationTeam/acceptToolList";
	}

	@GetMapping("/takenTools")
	public String takenTools( Model model) {
		List<Requests> acceptList = opService.acceptView(pjid);
		if(acceptList.size() >= 1) {
			opService.takenTools(acceptList,new Date());
		}
		return "redirect:/OperationTeam/acceptToolList?pj_id="+pjid;
	}
	
	  @PostMapping("/toolDamage")
	  public String toolDamage(@ModelAttribute("toolDamage") ToolAssetDamage toolAssetDamage) {		
		  opService.saveDamage(toolAssetDamage);
		  return "redirect:/OperationTeam/acceptToolList?tool_set_id=" + tool_set_id;
	 }
	  
	 
	@GetMapping("/returnTools") 
	public String returnTools(@RequestParam("req_id") int request_id, Model model) {
		Date currentDate = new Date();		
		Requests req = opService.getRequestById(request_id);		
		opService.returnTools(request_id,req.getAccept_qty(), currentDate);
		ToolAsset tool = opService.findToolById(req.getTool_asset_id());
		tool.setAvailable_qty(tool.getAvailable_qty() + req.getAccept_qty());
		opService.save(tool);
		return "redirect:/OperationTeam/acceptToolList?pj_id="+pjid;
	}

	@GetMapping("/showRejectList")
	public String Reject(Model model) {
		List<Requests> list = opService.findReject("Operation Manager");
		model.addAttribute("rejectpj", list);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/showRejectList";
	}

	@GetMapping("/rejectToolList")
	public String rejectToolList(@RequestParam("pj_id") int pj_id, Model model) {
		List<Requests> reject = opService.rejectView(pj_id);
		model.addAttribute("reject", reject);
		model.addAttribute("Emp_Name", Emp_Name);
		return "OperationTeam/rejectToolList";
	}

	/* code by amt for dept asset */
	@GetMapping("/showDeptAsset")
	public String showDeptAsset(@RequestParam("dept_id") int dept_id, Model model) {
		deptId = dept_id;
		List<Department> deptList = adminService.findAllDept();
		model.addAttribute("deptList", deptList);
		model.addAttribute("Emp_Name", Emp_Name);

		System.out.println("here it is" + dept_id);
		List<DeptFixedAsset> fixedAsset = departmentheadService.findFixedAsset(dept_id);
		model.addAttribute("deptFixedAsset", fixedAsset);

		List<DeptItAsset> itAsset = departmentheadService.findAllItAsset(dept_id);
		model.addAttribute("deptITAsset", itAsset);

		model.addAttribute("deptName", opService.findDeptById(dept_id).getDept_name());
		return "OperationTeam/showDeptAsset";
	}
	
	@GetMapping("/addAssetDept")
	public String addAssetDept(Model model) {
		List<FixedAsset> fixedAsset = adminService.findAllFixedAsset();
		model.addAttribute("fixedAssets",fixedAsset);
		DeptFixedAsset deptFixedAsset = new DeptFixedAsset();
		model.addAttribute("fixedAsset",deptFixedAsset);		
		List<ItAsset> itAsset = adminService.findAllItAsset();
		model.addAttribute("itAssets",itAsset);		
		List<ToolAsset> toolAsset = adminService.findAllToolAsset();
		model.addAttribute("toolAssets",toolAsset);
		model.addAttribute("deptName", opService.findDeptById(deptId).getDept_name());
		return "OperationTeam/addAssetDept";
	}
	
	@PostMapping("/addFixedAssetDept")
	public String addFixedAssetDept(@ModelAttribute("deptFixedAsset")DeptFixedAsset deptFixedAsset) {
		List<DeptFixedAsset> deptFixedAssetList = opService.getAllDeptFixedAsset();
		if(deptFixedAssetList.size() >= 1) {
			for(int i=0;i<deptFixedAssetList.size();i++) {
				if(deptFixedAssetList.get(i).getFixed_asset_id().equals(deptFixedAsset.getFixed_asset_id())) {
					deptFixedAssetList.get(i).setDept_id(deptFixedAsset.getDept_id());
					opService.saveDeptFixedAsset(deptFixedAssetList.get(i));
					return "redirect:/OperationTeam/fixedasset";
				}
			}
		}
		FixedAsset asset = opService.findById(deptFixedAsset.getFixed_asset_id());
		asset.setAvailable(false);
		opService.saveDeptFixedAsset(deptFixedAsset);
		return "redirect:/OperationTeam/fixedasset";
	}
	
	/*
	 * @PostMapping("/addFixedAssetDept") public String
	 * addFixedAssetDept(@ModelAttribute("fixedAsset") DeptFixedAsset
	 * deptFixedAsset) { int temp_deptid = deptId; String temp_assetid =
	 * deptFixedAsset.getFixed_asset_id(); List<DeptFixedAsset> list =
	 * opService.getAllDeptFixedAsset(); //list.get(0).setQty(3);
	 * 
	 * if(list.size() >= 1) { for(int i=0; i<list.size(); i++) {
	 * if(temp_deptid==list.get(i).getDept_id() &&
	 * temp_assetid.equals(list.get(i).getFixed_asset_id())) {
	 * list.get(i).setQty(list.get(i).getQty()+deptFixedAsset.getQty());
	 * //opService.saveDeptFixedAsset(deptFixedAsset); FixedAsset fixedAsset =
	 * opService.findById(deptFixedAsset.getFixed_asset_id());
	 * fixedAsset.setAvailable_qty(fixedAsset.getAvailable_qty()-deptFixedAsset.
	 * getQty()); opService.save(fixedAsset); return
	 * "redirect:/OperationTeam/addAssetDept"; } } }
	 * 
	 * deptFixedAsset.setDept_id(deptId);
	 * opService.saveDeptFixedAsset(deptFixedAsset); FixedAsset fixedAsset =
	 * opService.findById(deptFixedAsset.getFixed_asset_id());
	 * fixedAsset.setAvailable_qty(fixedAsset.getAvailable_qty()-deptFixedAsset.
	 * getQty()); opService.save(fixedAsset); return
	 * "redirect:/OperationTeam/addAssetDept"; }
	 */
	
	@GetMapping("/addItAssetDept")
	public String addItAssetDept(@RequestParam("it_asset_id") String it_asset_id, Model model) {
		DeptItAsset asset = new DeptItAsset(deptId,it_asset_id);
		opService.saveDeptItAsset(asset);
		return "redirect:/OperationTeam/addAssetDept";
	}
	
	@GetMapping("/addToolAssetDept")
	public String addToolAssetDept(@RequestParam("tool_asset_id") String tool_asset_id, Model model) {
		DeptToolAsset asset = new DeptToolAsset(deptId,tool_asset_id);
		opService.saveDeptToolAsset(asset);
		return "redirect:/OperationTeam/addAssetDept";
	}
	
	@GetMapping("/viewUserITAsset")
	public String viewUserITAsset(@RequestParam("id")String it_asset_id, Model model) {
		
		return "";
	}

}
