package com.teamA.demo.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.service.AdminService;
import com.teamA.demo.service.MemberService;
import com.teamA.demo.service.ProjectManagerService;

@Controller
@RequestMapping("/project_manager")
public class ProjectManagerController {
	public int pj_id;
	public int remove_id;
	public String accept_id;
	public String error="";
	private ProjectManagerService pmService;
	private AdminService adminService;
	private int emp_id;
	private int temp_id;
	private MemberService memberService;
	public ProjectManagerController(ProjectManagerService thepmService, AdminService adminS,MemberService mService) {
		pmService = thepmService;
		adminService = adminS;
		memberService=mService;
	}

	/* Project Manager Dashboard Project */

	@GetMapping("/pm_dashboard")
	public String Dashboard(Model model) {
		try {
			temp_id = (int) model.asMap().get("employeeID");
			if (temp_id == 0) {
				temp_id = emp_id;
			} else {
				emp_id = temp_id;
			}
		} catch (Exception e) {
			System.out.println("Temp Employee ID is null");
		}
		
		List<Project> projectList = pmService.getAllProjectList(emp_id);
		model.addAttribute("project", projectList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		
		return "project_manager/pm_dashboard";
	}

	@GetMapping("/profile")
	public String Profile(Model model) {
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		model.addAttribute("error", error);
		return "project_manager/profile";
	}
	
	@PostMapping("/changePassword")
	public String ChangePass(@RequestParam("current") String currentpass,@RequestParam("password1")String newpass,@RequestParam("password2")String confirmPass,Model model) {
		Employee profile = pmService.findById(emp_id);
		
		model.addAttribute("profile", profile);
		String pass=profile.getPassword();
		if(pass.equals(currentpass) && newpass.equals(confirmPass)) {
			profile.setPassword(newpass);
			pmService.saveProfile(profile);
			return "redirect:/project_manager/profile";
		}
		else {
			error="Something wrong!";
			
			return "redirect:/project_manager/profile";
		}
			}

	@GetMapping("/new_project")
	public String NewProject(Model model) {
		Project project = new Project();
		model.addAttribute("project", project);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/new_project";
	}
	

	@PostMapping("/saveProject")
	public String SaveProject(@ModelAttribute("project") Project project, Model model) {
		pmService.save(project);
		List<Project> projectList = pmService.getAllProjectList(emp_id);
		Employee profile = pmService.findById(emp_id);
		int pj_id=projectList.get(projectList.size()-1).getPj_id();
		int dept_id=profile.getDept_id();
		DeptProject dept_project=new DeptProject(dept_id,pj_id);
		pmService.saveDeptProject(dept_project);
		return "redirect:/project_manager/projectList";
	}
	@GetMapping("/Endproject")
	public String EndProject(@RequestParam("projectId")int pjid,Model model) {
		pmService.endProject(pjid);
		check();
		return "redirect:/project_manager/projectList";
	}
	
	@GetMapping("/addMembers")
	public String AddMember(Model model) {
		
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);

		int pmDept = profile.getDept_id();
		
		List<Employee> empList = pmService.getEmployeeList(pmDept);
		model.addAttribute("empLists", empList);
		

		return "project_manager/add_member_project";
	}

	@GetMapping("/add_members")
	public String AddMembersProject(@RequestParam("empId") int id, Model model) {
		remove_id=id;
		ProjectAssign pjAssign = new ProjectAssign(remove_id, pj_id);
		pmService.savePjAssign(pjAssign);
		Employee emp = pmService.findById(remove_id);
		emp.setAvailable(false);
		adminService.addEmployee(emp);
		return "redirect:/project_manager/addMembers";
	}

	@GetMapping("/projectList")
	public String ProjectList(Model model) {
		List<Project> projectList = pmService.getAllProjectList(emp_id);
		model.addAttribute("project", projectList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/pm_dashboard";
	}
	
	@GetMapping("/completeprojectList")
	public String CompleteProject(Model model) {
		List<Project> comPj=pmService.getCompletePj(emp_id);
		model.addAttribute("comPj", comPj);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/finishProjects";
	}

	@GetMapping("/view_member_in_project")
	public String ViewMember(@RequestParam("projectId") int pj_id1, Model model) {
		pj_id = pj_id1;
		List<ProjectAssign> memberList = pmService.MemberInProject(pj_id);
		model.addAttribute("members", memberList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/view_member_in_project";
	}
	@GetMapping("/viewToolInProject")
	public String ViewToolInProject(@RequestParam("projectId")int pj_id,Model model) {
		List<Requests> toolList=pmService.getPjTool(pj_id);
		model.addAttribute("toolList", toolList);
		
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/viewToolInProject";
	}

	
	@GetMapping("/view_member_inComProject")
	public String FinishPjEmp(@RequestParam("projectId") int pjid,Model model) {
		List<ProjectAssign> memberList = pmService.MemberInProject(pjid);
		model.addAttribute("members", memberList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/finishPjEmp";
	}

	@GetMapping("/view_members_in_project")
	public String View(Model model) {
		return "redirect:/project_manager/view_member_in_project?projectId=" + pj_id;
	}
	
	@GetMapping("/removeEmp")
	public String Remove(@RequestParam("removeId") int empid,Model model) {
		
		 Employee emp = pmService.findById(empid); 
		 emp.setAvailable(true);
		 adminService.addEmployee(emp);
		 
		pmService.deleteById(empid,pj_id);
		
		return "redirect:/project_manager/view_member_in_project?projectId=" + pj_id;
	}

	//This method will check projects that have ended and set available employee for new projects 
	public void check() { 
		Date currentDate = new Date();
		List<Employee> allEmp = adminService.findAllEmployee();
		for(int i=0; i<allEmp.size(); i++) {
			List<ProjectAssign> empProjectList = memberService.showProjectForMember(allEmp.get(i).getEmp_id());
			if(empProjectList.size() >= 1) {
				Project empProject = empProjectList.get(empProjectList.size()-1).getProjectEmp();
				Date pjEndDate = (Date)empProject.getEnd_date();
				if(currentDate.getTime() >= pjEndDate.getTime()) {
					allEmp.get(i).setAvailable(true);
					adminService.addEmployee(allEmp.get(i));
				}
			}				
		}
	}
	 
	/* end */

	/* ProjectManagerPending Process */
	@GetMapping("/pendingRequest")
	public String PendingRequest(Model model) {
		List<Requests> pendingList=pmService.getRequestList(emp_id);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		model.addAttribute("pendingList",pendingList);
		return "project_manager/pendingRequest";
	}

	@GetMapping("/view_tools")
	public String ViewTools(@RequestParam("toolSetId")String toolid,Model model) {
		accept_id=toolid;
		List<Requests> toolList=pmService.getAllRequest(accept_id);
		model.addAttribute("toolList", toolList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/view_request_tools";
	}
	
	@GetMapping("/acceptToolAction")
	public String AcceptTool(@RequestParam("request_id")int request_id) {
		pmService.acceptTool(request_id);
		return "redirect:/project_manager/view_tools?toolSetId=" + accept_id;
	}
	@GetMapping("/rejectToolAction")
	public String RejectTool(@RequestParam("request_id")int request_id) {
		pmService.rejectTool(request_id);
		return "redirect:/project_manager/view_tools?toolSetId=" + accept_id;
	}
	/* end */
	
	/*Accept process*/
	
	@GetMapping("/acceptRequest")
	public String AcceptRequest(Model model) {
		List<Requests> acceptList=pmService.getAcceptList(emp_id);
		model.addAttribute("acceptList", acceptList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		
		return "project_manager/acceptRequest";
	}
	@GetMapping("/view_tools_accept")
	public String ViewAcceptTool(@RequestParam("acceptToolId")int acceptToolId,Model model) {
		List<Requests> acceptToolList=pmService.getAcceptTool(acceptToolId);
		model.addAttribute("acceptToolList",acceptToolList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/acceptTools";
	}
	/*end*/
	
	/*Reject Process*/
	@GetMapping("/rejectRequest")
	public String RejectRequest(Model model) {
		List<Requests> rejectList=pmService.getRejectList(emp_id);
		model.addAttribute("rejectList", rejectList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/rejectRequest";
	}
	@GetMapping("/view_tools_reject")
	public String ViewRejectTool(@RequestParam("rejectToolId")int rejectToolId,Model model) {
		List<Requests> rejectToolList=pmService.getRejectTool(rejectToolId);
		model.addAttribute("rejectToolList", rejectToolList);
		Employee profile = pmService.findById(emp_id);
		model.addAttribute("profile", profile);
		return "project_manager/rejectTools";
	}
	/*end*/

}
