package com.teamA.demo.dao;

import java.util.List;

import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;

public interface AdminDao {
public List<Project> findAll();
	
	public void addNewDept(Department theDepartment);
	public List<Department> findAllDept();
	List<Employee> showDeptMember(int dept_id);
	 List<DeptFixedAsset> DeptFixedAsset(int dept_id);
	 List<DeptItAsset> DeptItAsset(int dept_id);
	 List<DeptProject> DeptProject(int dept_id);
	
	
	void addEmployee(Employee theEmployee);
	Employee findById(int emp_id);
	List<Employee> findAllEmployee();

	

	List<ProjectAssign> showMemberList(int pj_id);
	
	public List<FixedAsset> findAllFixedAsset();
	
	public List<ItAsset> findAllItAsset();
	
	public List<ToolAsset> findAllToolAsset();



}
