package com.teamA.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.ToolAsset;


@Repository
public class AdminDaoImpl implements AdminDao{
	
private EntityManager entityManager;
	
	@Autowired
	public AdminDaoImpl(EntityManager entity) {
		entityManager=entity;
		
	}
	
	@Override
	@Transactional
	public List<Project> findAll() {
		//get the current session
		Session currentSession = entityManager.unwrap(Session.class);
		
		//create query
		Query<Project> theQuery=currentSession.createQuery("from Project",Project.class);
		
		//get result list
		List<Project> projects=theQuery.getResultList();
		System.out.print(projects.get(0));
		
		return projects;
	}

	@Override
	@Transactional
	public void addNewDept(Department theDepartment) {
		Session currentSession=entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theDepartment);
		
	}

	@Override
	@Transactional
	public List<Employee> findAllEmployee() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee",Employee.class);
		List<Employee> employees = theQuery.getResultList();
		return employees;
	}
	
	public List<Department> findAllDept() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Department> theQuery = currentSession.createQuery("from Department where dept_id != 5",Department.class);
		List<Department> departments = theQuery.getResultList();
		return departments;
	}

	@Override
	public Employee findById(int emp_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Employee theemployee = currentSession.get(Employee.class, emp_id);
		return theemployee;
	}

	@Override
	@Transactional
	@Modifying
	public void addEmployee(Employee theEmployee) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theEmployee);
		
	}

	@Override
	public List<ProjectAssign> showMemberList(int pj_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ProjectAssign> theQuery = currentSession.createQuery("from ProjectAssign where pj_id=:id",ProjectAssign.class);
		theQuery.setParameter("id", pj_id);
		List<ProjectAssign> projectListForMember = theQuery.getResultList();
		return projectListForMember;
	}
	
	@Override
	public List<FixedAsset> findAllFixedAsset() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<FixedAsset> theQuery = currentSession.createQuery("from FixedAsset",FixedAsset.class);
		List<FixedAsset> fixedassetlist = theQuery.getResultList();
		return fixedassetlist;
	}

	@Override
	public List<ItAsset> findAllItAsset() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ItAsset> theQuery = currentSession.createQuery("from ItAsset",ItAsset.class);
		List<ItAsset> itassetlist = theQuery.getResultList();
		return itassetlist;
	}

	@Override
	public List<ToolAsset> findAllToolAsset() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ToolAsset> theQuery = currentSession.createQuery("from ToolAsset",ToolAsset.class);
		List<ToolAsset> toolassetlist = theQuery.getResultList();
		return toolassetlist;
	}

	@Override
	public List<Employee> showDeptMember(int dept_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee where dept_id=:id",Employee.class);
		theQuery.setParameter("id", dept_id);
		List<Employee> deptMemberList = theQuery.getResultList();
		return deptMemberList;
	}

	@Override
	public List<DeptFixedAsset> DeptFixedAsset(int dept_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptFixedAsset> theQuery = currentSession.createQuery("from DeptFixedAsset where dept_id=:id",DeptFixedAsset.class);
		theQuery.setParameter("id", dept_id);
		List<DeptFixedAsset> deptFixAsset = theQuery.getResultList();
		return deptFixAsset;
	}

	@Override
	public List<DeptItAsset> DeptItAsset(int dept_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptItAsset> theQuery = currentSession.createQuery("from DeptItAsset where dept_id=:id",DeptItAsset.class);
		theQuery.setParameter("id", dept_id);
		List<DeptItAsset> deptItAsset = theQuery.getResultList();
		return deptItAsset;
	}

	@Override
	public List<DeptProject> DeptProject(int dept_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptProject> theQuery = currentSession.createQuery("from DeptProject where dept_id=:id",DeptProject.class);
		theQuery.setParameter("id", dept_id);
		List<DeptProject> deptPrj= theQuery.getResultList();
		return deptPrj;
	}



	
	}


