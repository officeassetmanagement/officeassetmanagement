package com.teamA.demo.dao;

import java.util.List;

import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;

public interface DepartmentHeadDao {
	
	public List<DeptProject> findProjectForEachDepartment(int dept_id);
	public List<Employee> findEmployeeList(int dept_id);
	public List<Requests> findAccept(int dept_id);
	
	public List<Requests> findPendingProject(int dept_id);
	
	public List<Requests> findReject(int dept_id) ;
	public List<DeptFixedAsset> findFixedAsset(int dept_id);
	public List<DeptItAsset> findAllItAsset(int dept_id);
	public List<Requests> ReuestToolList(String tool_set_id);
	public List<Requests> AcceptViewTools(int pj_id);
	public List<Requests> RejectViewTools(int pj_id);
	public List<Requests> RequestToolLists(String tool_set_id);
	public void AcceptHistoryTool(int request_id);
	public void RejectHistoryTool(int request_id);
	 
	 
	 
	 
}
