package com.teamA.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;






@Repository
public class DepartmentHeadDaoImpl implements DepartmentHeadDao {
	
	private EntityManager entityManager;
	@Autowired
	public DepartmentHeadDaoImpl(EntityManager entitymanager) {
		entityManager = entitymanager;
	}
	

	@Override
	@Transactional
	public List<DeptProject> findProjectForEachDepartment(int dept_id){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptProject> theQuery = currentSession.createQuery("from DeptProject where dept_id=:id",DeptProject.class).setParameter("id", dept_id);
		List<DeptProject> pjlists = theQuery.getResultList();
		return pjlists;
	}
	
	@Override
	@Transactional
	public List<Employee> findEmployeeList(int dept_id) {
Session currentSession = entityManager.unwrap(Session.class);
		
		//create a query
		Query<Employee> theQuery = currentSession.createQuery("from Employee where dept_id=:id",Employee.class).setParameter("id",dept_id);
		
		//excute query and get result list
		List<Employee> employeeslists = theQuery.getResultList();
		
		//return the results
		return employeeslists;
	}


	@Override
	@Transactional
	public List<Requests> findAccept(int dept_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Department Head' and rejectby='' and empRequests.dept_id=:id group by pj_id",Requests.class);
		theQuery.setParameter("id",dept_id);
		List<Requests> acceptpj = theQuery.getResultList();
		return acceptpj;
	}

	@Override
	@Transactional
	public List<Requests> findReject(int dept_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Project Manager' and rejectby='Department Head' and empRequests.dept_id=:id group by pj_id",Requests.class);
		theQuery.setParameter("id",dept_id);
		List<Requests> rejectpj = theQuery.getResultList();
		return rejectpj;
	}

	 //for asset
	@Override
	@Transactional
	public List<DeptFixedAsset> findFixedAsset(int dept_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptFixedAsset> theQuery = currentSession.createQuery("from DeptFixedAsset where dept_id=:id",DeptFixedAsset.class).setParameter("id",dept_id);
		List<DeptFixedAsset> fixedAsset = theQuery.getResultList();
		return fixedAsset;
	}


	@Override
	@Transactional
	public List<DeptItAsset> findAllItAsset(int dept_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<DeptItAsset> theQuery = currentSession.createQuery("from DeptItAsset where dept_id=:id",DeptItAsset.class).setParameter("id",dept_id);
		List<DeptItAsset> itAsset = theQuery.getResultList();
		return itAsset;
	}

	@Override
	@Transactional
	public List<Requests> ReuestToolList(String tool_set_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Project Manager' and rejectby='' and tool_set_id=:id",Requests.class).setParameter("id", tool_set_id);
		List<Requests> toolList = theQuery.getResultList();
		return toolList;
	}


	@Override
	@Transactional
	public List<Requests> AcceptViewTools(int pj_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Department Head' and rejectby='' and pj_id=:id",Requests.class).setParameter("id",pj_id);
		List<Requests> toolList = theQuery.getResultList();
		return toolList;
	}


	@Override
	public List<Requests> findPendingProject(int dept_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> pendingPj = currentSession.createQuery("from Requests where acceptby='Project Manager' and rejectby='' and empRequests.dept_id=:id group by tool_set_id",Requests.class).setParameter("id", dept_id).getResultList();
		return pendingPj;
	}


	@Override
	public List<Requests> RejectViewTools(int pj_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Project Manager' and rejectby='Department Head' and pj_id=:id",Requests.class).setParameter("id",pj_id);
		List<Requests> rejecttoollist = theQuery.getResultList();
		return rejecttoollist;
	}


	@Override
	public List<Requests> RequestToolLists(String tool_set_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where tool_set_id=:id and acceptby='' and rejectby='' ",Requests.class).setParameter("id",tool_set_id);
		List<Requests> toollist = theQuery.getResultList();
		return toollist;
	}


	@Override
	@Transactional
	@Modifying
	public void AcceptHistoryTool(int request_id) {
		// TODO Auto-generated method stub
		Session curSession=entityManager.unwrap(Session.class);
		String hql_update="update Requests set acceptby ='Department Head' where request_id=:id";
		Query query=curSession.createQuery(hql_update);
		query.setParameter("id", request_id);
		query.executeUpdate();
	}


	@Override
	@Transactional
	@Modifying
	public void RejectHistoryTool(int request_id) {
		// TODO Auto-generated method stub
		Session curSession=entityManager.unwrap(Session.class);
		String hql_update="update Requests set rejectby ='Department Head' where acceptby='Project Manager' and request_id=:id";
		Query query=curSession.createQuery(hql_update);
		query.setParameter("id", request_id);
		query.executeUpdate();
	}

}


	 
	

   
	


