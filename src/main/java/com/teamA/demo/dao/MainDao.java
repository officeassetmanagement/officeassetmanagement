package com.teamA.demo.dao;

import java.util.List;

import com.teamA.demo.entity.Employee;

public interface MainDao {

	public List<Employee> findUser();

}
