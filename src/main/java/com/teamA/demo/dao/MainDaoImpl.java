package com.teamA.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.teamA.demo.entity.Employee;

@Repository
public class MainDaoImpl implements MainDao {
	private EntityManager entityManager;
	
	@Autowired
	public MainDaoImpl(EntityManager entity) {
		entityManager = entity;
	}
	
	@Override
	public List<Employee> findUser(){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<Employee> query=currentSession.createQuery("from Employee where end_date = null ",Employee.class);
		List<Employee> employee=query.getResultList();
		return employee;
	}
	
	/*
	 * @Override public Employee findUser(String email,String password) { Session
	 * currentSession=entityManager.unwrap(Session.class); Employee
	 * employee=currentSession.get(Employee.class,email,password); return employee;
	 * }
	 */

}
