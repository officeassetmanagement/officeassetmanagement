package com.teamA.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;

@Repository
public class MemberDaoImpl implements MemberDao{
	
	private EntityManager entityManager;
	
	@Autowired
	public MemberDaoImpl(EntityManager entity) {
		entityManager=entity;
	}

	@Override
	@Transactional
	public List<ProjectAssign> showProjectForMember(int emp_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ProjectAssign> theQuery = currentSession.createQuery("from ProjectAssign where emp_id=:id",ProjectAssign.class);
		theQuery.setParameter("id", emp_id);
		List<ProjectAssign> projectListForMember = theQuery.getResultList();
		return projectListForMember;
	}
	@Transactional
	@Modifying
	public void removeById(int reqId) {
		Session curSession=entityManager.unwrap(Session.class);
		Query remove=curSession.createQuery("delete from Requests where request_id=:rid");
		remove.setParameter("rid", reqId);
		remove.executeUpdate();
	}
	@Transactional
	@Modifying
	public void removeRequest(String tsId) {
		Session curSession=entityManager.unwrap(Session.class);
		Query removeReq=curSession.createQuery("delete from Requests where tool_set_id=:tid");
		removeReq.setParameter("tid", tsId);
		removeReq.executeUpdate();
	}
	

	@Override
	public Project showProjectDetail(int pj_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Project projectDetail = currentSession.get(Project.class, pj_id);
		return projectDetail;
	}

	@Override
	public List<Requests> showPending(int emp_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> pendings = currentSession.createQuery("from Requests where emp_id=:id and acceptby !='Operation Manager' and rejectby = '' ",Requests.class).setParameter("id", emp_id).getResultList();
		return pendings;
	}

	@Override
	public List<Requests> showAccept(int emp_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where emp_id=:id and acceptby='Operation Manager'",Requests.class).setParameter("id", emp_id);
		List<Requests> accept=theQuery.getResultList();	
		return accept;
	}

	@Override
	public Employee showOperationManager(int roleName) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee where role_name=:role",Employee.class).setParameter("role", roleName);
		Employee op=theQuery.getSingleResult();
	
		return op;
	}

	@Override
	public List<Requests> showReject(int emp_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> rejects = currentSession.createQuery("from Requests where emp_id=:id and rejectby!=''",Requests.class).setParameter("id", emp_id).getResultList();
		return rejects;
	}

	@Override
	public List<Requests> showRequests(int pj_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> requests = currentSession.createQuery("from Requests where pj_id=:id",Requests.class).setParameter("id", pj_id).getResultList();
		return requests;
	}

	@Override
	public List<Requests> showAcceptProject(int pj_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where pj_id=:id and acceptby='Operation Manager'",Requests.class).setParameter("id", pj_id);
		List<Requests> acceptProject=theQuery.getResultList();	
		return acceptProject;
	}

	@Override
	public List<Requests> showRejectProject(int pj_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> rejectProject = currentSession.createQuery("from Requests where pj_id=:id and rejectby!=''",Requests.class).setParameter("id", pj_id).getResultList();
		return rejectProject;
	}

	@Override
	public List<ToolAsset> showToolAssetList() {
		Session currentSession = entityManager.unwrap(Session.class);
		List<ToolAsset> ToolAsset = currentSession.createQuery("from ToolAsset " ,ToolAsset.class).getResultList();
		return ToolAsset;
	}

	@Override
	public void saveRequest(Requests req) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(req);
	}
	
	@Override
	public List<Requests> showAllRequest() {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> allRequest = currentSession.createQuery("from Requests",Requests.class).getResultList();
		return allRequest;
	}

	@Override
	public List<Requests> showRequestTools(String ts_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> requestTools = currentSession.createQuery("from Requests where tool_set_id=:id",Requests.class).setParameter("id", ts_id).getResultList();
		return requestTools;
	}

	@Override
	public Requests getRequestTool(String toolId, int pj_ID) {
		Session curSes=entityManager.unwrap(Session.class);
		Query<Requests> tool=curSes.createQuery("from Requests where pj_id=:iid and tool_asset_id=:tid",Requests.class);
		tool.setParameter("iid", pj_ID);
		tool.setParameter("tid", toolId);
		Requests tools=tool.getSingleResult();
		return tools;
	}

}
