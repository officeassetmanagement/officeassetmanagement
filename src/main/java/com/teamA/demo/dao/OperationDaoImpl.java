package com.teamA.demo.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.EmployeeAsset;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.ToolAssetDamage;

@Repository
public class OperationDaoImpl implements OperationDao{

private EntityManager entityManager;
	
	@Autowired
	public OperationDaoImpl(EntityManager entity) {
		entityManager=entity;
	}

	@Override
	public List<FixedAsset> findAll() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<FixedAsset> theQuery = currentSession.createQuery("from FixedAsset",FixedAsset.class);
		List<FixedAsset> fixedAsset = theQuery.getResultList();
		return fixedAsset;
	}
	
	@Override
	public void deleteById(String fixed_asset_id) {
		Session currentSession=entityManager.unwrap(Session.class);
		Query<FixedAsset> theQuery = currentSession.createQuery("delete from FixedAsset where fixed_asset_id=:fixedId");
		theQuery.setParameter("fixedId", fixed_asset_id);
		theQuery.executeUpdate();
		
	}

	@Override
	public FixedAsset findById(String fixed_asset_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		FixedAsset fixedAsset=currentSession.get(FixedAsset.class,fixed_asset_id);
		return fixedAsset;
	}
	public List<Employee> getAllEmployee(){
		Session curSession=entityManager.unwrap(Session.class);
		return curSession.createQuery("from Employee where role_name!='Admin'",Employee.class).getResultList();
	}
	public List<ItAsset> getDevices(){
		Session curSession=entityManager.unwrap(Session.class);
		return curSession.createQuery("from ItAsset where (type='Laptop' or type='Tablet') and available=true",ItAsset.class).getResultList();
	}
	@Override
	public void save(FixedAsset fixedAsset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(fixedAsset);		
	}
	@Transactional
	@Modifying
	public void acceptById(int req_id) {
		Session curSession=entityManager.unwrap(Session.class);
		String hql="update Requests set acceptby='Operation Manager' where request_id=:iid";
		Query update=curSession.createQuery(hql);
		update.setParameter("iid", req_id);
		update.executeUpdate();
	}
	@Transactional
	@Modifying
	public void rejectById(int req_id) {
		Session curSession=entityManager.unwrap(Session.class);
		String hql="update Requests set rejectby='Operation Manager',reject_qty=request_qty where request_id=:idd";
		Query update=curSession.createQuery(hql);
		update.setParameter("idd", req_id);
		update.executeUpdate();
	}

	@Override
	public List<ItAsset> findAllIT() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ItAsset> theQuery = currentSession.createQuery("from ItAsset",ItAsset.class);
		List<ItAsset> itAsset = theQuery.getResultList();
		return itAsset;
		
	}

	@Override
	public void save(ItAsset asset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(asset);
		
	}

	@Override
	public ItAsset findByITId(String it_asset_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		ItAsset itAsset=currentSession.get(ItAsset.class,it_asset_id);
		return itAsset;
	}

	@Override
	
	public void deleteByITId(String it_asset_id) {
		Session currentSession=entityManager.unwrap(Session.class);
		Query<ItAsset> theQuery = currentSession.createQuery("delete from ItAsset where it_asset_id=:itId");
		theQuery.setParameter("itId", it_asset_id);
		theQuery.executeUpdate();
		
	}

	@Override
	public List<ToolAsset> findAllTool() {
		Session currentSession=entityManager.unwrap(Session.class);
		Query<ToolAsset> theQuery = currentSession.createQuery("from ToolAsset",ToolAsset.class);
		List<ToolAsset> toolAsset = theQuery.getResultList();
		return toolAsset;		
	}

	@Modifying
	@Override
	public void save(ToolAsset toolAsset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(toolAsset);		
	}

	@Override
	public ToolAsset findToolById(String tool_asset_id) {
		Session curSession=entityManager.unwrap(Session.class);
		ToolAsset toolAsset=curSession.get(ToolAsset.class, tool_asset_id);
		return toolAsset;
	}

	@Override
	public void deleteToolById(String tool_asset_id) {
		Session currentSession=entityManager.unwrap(Session.class);
		Query<ToolAsset> theQuery=currentSession.createQuery("delete from ToolAsset where tool_asset_id=:toolId");
		theQuery.setParameter("toolId", tool_asset_id);
		theQuery.executeUpdate();		
	}

	@Override
	public List<ToolAssetDamage> getDamageTools() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ToolAssetDamage> theQuery = currentSession.createQuery("from ToolAssetDamage",ToolAssetDamage.class);
		List<ToolAssetDamage> damageTool = theQuery.getResultList();
		return damageTool;
	}

	@Override
	public List<Requests> getRequest() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby='Department Head' and rejectby='' group by tool_set_id",Requests.class);
		List<Requests> request = theQuery.getResultList();
		return request;
	}

	@Override
	@Transactional
	public List<Requests> findAccept(String acceptby) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where acceptby=:name group by pj_id",Requests.class);
		theQuery.setParameter("name",acceptby );
		List<Requests> acceptpj = theQuery.getResultList();
		return acceptpj;
	}

	@Override
	public List<Requests> findReject(String rejectby) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> theQuery = currentSession.createQuery("from Requests where rejectby=:name group by tool_set_id",Requests.class);
		theQuery.setParameter("name",rejectby );
		List<Requests> rejectpj = theQuery.getResultList();
		return rejectpj;
	}
	
	@Override
	public Department findDeptById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from Department where dept_id=:id",Department.class).setParameter("id", id).getSingleResult();
	}
	
	@Override
	public List<Requests> rejectView(int ts_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> reject = currentSession.createQuery("from Requests where rejectby='Operation Manager' and  pj_id=:id",Requests.class).setParameter("id", ts_id).getResultList();
		return reject;
	}

	@Override
	public List<Requests> acceptView(int ts_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> accept = currentSession.createQuery("from Requests where acceptby='Operation Manager' and  pj_id=:id",Requests.class).setParameter("id", ts_id).getResultList();
		return accept;
	}

	@Override
	public List<Requests> pendingView(String ts_id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		List<Requests> pendings = currentSession.createQuery("from Requests where acceptby='Department Head' and rejectby='' and  tool_set_id=:id",Requests.class).setParameter("id", ts_id).getResultList();
		return pendings;
	}

	@Override
	public int findId(String tool_id, String tool_set_id) {
		// TODO Auto-generated method stub
		Session currentSession=entityManager.unwrap(Session.class);
		String hql_select="from Requests where tool_asset_id=:tool_id1 and tool_set_id=:tool_set_id1";
		Query<Requests> theQuery=currentSession.createQuery(hql_select,Requests.class);
		theQuery.setParameter("tool_id1", tool_id);
		theQuery.setParameter("tool_set_id1",tool_set_id);
		Requests request=theQuery.getSingleResult();
		return request.getRequest_id();
	}

	@Override
	public void saveDamage(ToolAssetDamage damage) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(damage);
	}

	@Override
	@Modifying
	@Transactional
	public void saveDeptFixedAsset(DeptFixedAsset asset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(asset);
	}

	@Override
	public List<DeptFixedAsset> getAllDeptFixedAsset() {
		Session currentSession = entityManager.unwrap(Session.class);
		List<DeptFixedAsset> list = currentSession.createQuery("from DeptFixedAsset",DeptFixedAsset.class).getResultList();
		return list;
	}

	@Override
	public void saveDeptItAsset(DeptItAsset asset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(asset);
	}

	@Override
	public List<DeptItAsset> getAllDeptItAsset() {
		Session currentSession = entityManager.unwrap(Session.class);
		List<DeptItAsset> list = currentSession.createQuery("from DeptItAsset",DeptItAsset.class).getResultList();
		return list;
	}

	@Override
	public void saveDeptToolAsset(DeptToolAsset asset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(asset);
	}

	@Override
	public Requests getRequestById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from Requests where request_id=:id",Requests.class).setParameter("id",id).getSingleResult();
	}

	@Transactional
	@Override
	public void returnTools(int request_id,int return_qty, Date currentDate) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Requests> query = currentSession.createQuery("update Requests set return_date=:dd,return_qty=:qty where request_id=:id");
		query.setParameter("dd", currentDate);
		query.setParameter("id", request_id);
		query.setParameter("qty", return_qty);
		query.executeUpdate();
	}

	@Override
	public void saveUserDevice(EmployeeAsset empAsset) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(empAsset);
	}

	@Override
	public List<EmployeeAsset> getUsedDevices() {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from EmployeeAsset",EmployeeAsset.class).getResultList();
	}

	@Override
	public List<Requests> showRequestsById(String tool_asset_id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from Requests where return_date=null and acceptby='Operation Manager' and tool_asset_id=:id",Requests.class).setParameter("id", tool_asset_id).getResultList();
	}

	@Override
	public List<Requests> showHistory() {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from Requests where acceptby='Operation Manager'",Requests.class).getResultList();
	}

	@Override
	public List<EmployeeAsset> showUserDevices(int empId) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from EmployeeAsset where emp_id=:id",EmployeeAsset.class).setParameter("id", empId).getResultList();
	}
	@Override
	public EmployeeAsset getEmployeeAssetById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.createQuery("from EmployeeAsset where id=:id",EmployeeAsset.class).setParameter("id", id).getSingleResult();
	}

	@Modifying
	@Transactional
	@Override
	public void removeUserDevice(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query remove = currentSession.createQuery("delete from EmployeeAsset where id=:id").setParameter("id", id);
		remove.executeUpdate();
	}

	@Modifying
	@Transactional
	@Override
	public void removeDeptItAsset(int dept_id, String it_asset_id) {
		Query remove = entityManager.unwrap(Session.class).createQuery("delete from DeptItAsset where dept_id=:dept_id and it_asset_id=:itid");
		remove.setParameter("dept_id", dept_id);
		remove.setParameter("itid", it_asset_id);
		remove.executeUpdate();
	}

	@Override
	public List<EmployeeAsset> showUsers(String it_asset_id) {
		return entityManager.unwrap(Session.class).createQuery("from EmployeeAsset where it_asset_id=:id",EmployeeAsset.class).setParameter("id", it_asset_id).getResultList();
	}

	@Modifying
	@Transactional
	@Override
	public void takenTools(List<Requests> acceptList, Date date) {
		Session currentSession = entityManager.unwrap(Session.class);
		for(int i=0; i<acceptList.size(); i++) {
			Query query = currentSession.createQuery("update Requests set out_date=:dd where request_id=:id");
			query.setParameter("dd", date);
			query.setParameter("id", acceptList.get(i).getRequest_id());
			query.executeUpdate();
		}
	}
	
}
