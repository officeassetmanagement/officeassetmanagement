package com.teamA.demo.dao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;

@Repository
public class ProjectManagerDaoImpl implements ProjectManagerDao {
	
	private EntityManager entityManager;
	
	@Autowired
	public ProjectManagerDaoImpl(EntityManager entity) {
		entityManager=entity;
	}
	@Override
	@Transactional
	public List<Project> getAllProject(int emp_id){
		Session currentSession= entityManager.unwrap(Session.class);
		
		Query<Project> theQuery=currentSession.createQuery("from Project where emp_id=:id and end_date=null",Project.class);
		theQuery.setParameter("id",emp_id);
		List<Project> project=theQuery.getResultList();
		return project;
	}
	public List<Project> getCompletePj(int emp_id){
		Session currentSession= entityManager.unwrap(Session.class);
		
		Query<Project> theQuery=currentSession.createQuery("from Project where emp_id=:id and end_date!=null",Project.class);
		theQuery.setParameter("id",emp_id);
		
		List<Project> comPj=theQuery.getResultList();
		return comPj;
	}
	
	public void save(Project project) {
		Session currentSession=entityManager.unwrap(Session.class);
		currentSession.save(project);
	}
	@Transactional
	@Modifying
	public void endProject(int pjid) {
		Session curSession=entityManager.unwrap(Session.class);
		Date cur_date=new Date();
		String end_hql="update Project set end_date=:cur_date where pj_id=:pid";
		Query query=curSession.createQuery(end_hql);
		query.setParameter("cur_date", cur_date);
		query.setParameter("pid", pjid);
		query.executeUpdate();
	}
	public void saveDeptProject(DeptProject dept_project) {
		Session curSession=entityManager.unwrap(Session.class);
		curSession.save(dept_project);
	}
	@Transactional
	public void saveProfile(Employee employee) {
		Session currentSession=entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(employee);
	}
	
	public void savePjAssign(ProjectAssign pjAssign) {
		Session currentSession=entityManager.unwrap(Session.class);
		currentSession.save(pjAssign);
	}
	
	public Employee findById(int emp_id) {
		Session currentSession=entityManager.unwrap(Session.class);
		Employee employee=currentSession.get(Employee.class, emp_id);
		return employee;
	}
	
	public List<Requests> getAllRequest(String accept_id){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<Requests> queryRequest=currentSession.createQuery("from Requests where tool_set_id=:id and acceptby='' and rejectby='' ",Requests.class);
		queryRequest.setParameter("id", accept_id);
		List<Requests> request=queryRequest.getResultList();
		return request;
	}
	public List<Requests> getPjTool(int pj_id){
		Session curSession=entityManager.unwrap(Session.class);
		Query<Requests> toolList=curSession.createQuery("from Requests where pj_id=:id and acceptby='Operation Manager'",Requests.class);
		toolList.setParameter("id", pj_id);
		List<Requests> tool=toolList.getResultList();
		return tool;
	}
	public List<ProjectAssign> MemberInProject(int pj_id){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<ProjectAssign> theQuery=currentSession.createQuery("from ProjectAssign where pj_id=:id",ProjectAssign.class);
		theQuery.setParameter("id",pj_id);
		List<ProjectAssign> members=theQuery.getResultList();
		return members;
	}
	
	public List<Employee> getEmployeeList(int dept_id){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<Employee> query=currentSession.createQuery("from Employee E where E.role_name='Member' and E.dept_id=:id and E.available=true and E.end_date=null ",Employee.class);
		query.setParameter("id", dept_id);
		List<Employee> employeeList=query.getResultList();
		return employeeList;
	}
	@Transactional
	@Modifying
	public void deleteById(int id,int pjid) {
		Session currentSession=entityManager.unwrap(Session.class);
		Query delete=currentSession.createQuery("delete from ProjectAssign where emp_id=:idd and pj_id=:pid ");
		delete.setParameter("idd", id);
		delete.setParameter("pid", pjid);
		delete.executeUpdate();
		
	}
	
	public List<Requests> getRequestList(int emp_id){
		
		Session currentSession=entityManager.unwrap(Session.class);
		//String hql="select distinct P.pj_name,R.empRequests.emp_name,R.date_time,R.tool_set_id from Project P inner join Requests R on P.pj_id=R.pj_id and P.emp_id=:eid and R.acceptby='' and R.rejectby='' ";
		Query query=currentSession.createQuery("from Requests where acceptby='' and rejectby='' and projectRequests.emp_id=:id group by tool_set_id",Requests.class);
		query.setParameter("id", emp_id);
		List<Requests> requestList=query.getResultList();
		return requestList;
	}
	public List<Requests> getAcceptList(int emp_id){
		Session currSession=entityManager.unwrap(Session.class);
		Query query=currSession.createQuery("from Requests where acceptby='Project Manager' and rejectby='' and projectRequests.emp_id=:id group by pj_id",Requests.class);
		query.setParameter("id", emp_id);
		List<Requests> acceptList=query.getResultList();
		return acceptList;
	}
	public List<Requests> getRejectList(int emp_id){
		Session currSession=entityManager.unwrap(Session.class);
		Query query=currSession.createQuery("from Requests where acceptby='' and rejectby='Project Manager' and projectRequests.emp_id=:id group by pj_id",Requests.class);
		query.setParameter("id", emp_id);
		List<Requests> rejectList=query.getResultList();
		return rejectList;
	}
	@Transactional
	@Modifying
	public void acceptTool(int request_id) {
		Session curSession=entityManager.unwrap(Session.class);
		String hql_update="update Requests set acceptby ='Project Manager' where request_id=:id";
		Query query_up=curSession.createQuery(hql_update);
		query_up.setParameter("id", request_id);
		query_up.executeUpdate();
	}
	@Transactional
	@Modifying
	public void rejectTool(int request_id) {
		Session curSession=entityManager.unwrap(Session.class);
		String hql_update="update Requests set rejectby ='Project Manager' where request_id=:id";
		Query query_up=curSession.createQuery(hql_update);
		query_up.setParameter("id", request_id);
		query_up.executeUpdate();
	}
	/* acceptToolList */
	public List<Requests> getAcceptTool(int acceptToolId){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<Requests> queryRequest=currentSession.createQuery("from Requests where pj_id=:id and (acceptby='Project Manager' or acceptby='Department Head' or acceptby='Operation Manager')",Requests.class);
		queryRequest.setParameter("id", acceptToolId);
		List<Requests> acceptTool=queryRequest.getResultList();
		return acceptTool;
	}
	public List<Requests> getRejectTool(int rejectToolId){
		Session currentSession=entityManager.unwrap(Session.class);
		Query<Requests> queryRequest=currentSession.createQuery("from Requests where pj_id=:id and (rejectby='Project Manager' or rejectby='Department Head' or rejectby='Operation Manager')",Requests.class);
		queryRequest.setParameter("id", rejectToolId);
		List<Requests> rejectTool=queryRequest.getResultList();
		return rejectTool;
	}
}
