package com.teamA.demo.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="department")
public class Department {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="dept_name")
	private String dept_name;
	
	@OneToMany(mappedBy = "departmentEmp")
	private List<Employee> employeeList;
	
	@OneToMany(mappedBy = "departmentPj")
	private List<DeptProject> deptProjectList;
	
	@OneToMany(mappedBy = "departmentItAsset")
	private List<DeptItAsset> deptItAssetList;
	
	@OneToMany(mappedBy = "departmentFixedAsset")
	private List<DeptFixedAsset> deptFixedAssetList;
	
	public Department() {
		
	}

	public Department(int dept_id, String dept_name) {
		super();
		this.dept_id = dept_id;
		this.dept_name = dept_name;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<DeptProject> getDeptProjectList() {
		return deptProjectList;
	}

	public void setDeptProjectList(List<DeptProject> deptProjectList) {
		this.deptProjectList = deptProjectList;
	}
	
	

}
