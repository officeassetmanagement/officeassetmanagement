package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="dept_fixed_asset")
public class DeptFixedAsset {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="fixed_asset_id")
	private String fixed_asset_id;
	
	@ManyToOne
	@JoinColumn(name = "dept_id", referencedColumnName = "dept_id",insertable=false, updatable=false)
	private Department departmentFixedAsset;
	
	@OneToOne
	@JoinColumn(name = "fixed_asset_id", referencedColumnName = "fixed_asset_id",insertable=false, updatable=false)
	private FixedAsset fixedAssetDept;
	
	public DeptFixedAsset() {
		
	}

	public DeptFixedAsset(int id, int dept_id, String fixed_asset_id) {
		super();
		this.id = id;
		this.dept_id = dept_id;
		this.fixed_asset_id = fixed_asset_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getFixed_asset_id() {
		return fixed_asset_id;
	}

	public void setFixed_asset_id(String fixed_asset_id) {
		this.fixed_asset_id = fixed_asset_id;
	}

	public Department getDepartmentFixedAsset() {
		return departmentFixedAsset;
	}

	public void setDepartmentFixedAsset(Department departmentFixedAsset) {
		this.departmentFixedAsset = departmentFixedAsset;
	}

	public FixedAsset getFixedAssetDept() {
		return fixedAssetDept;
	}

	public void setFixedAssetDept(FixedAsset fixedAssetDept) {
		this.fixedAssetDept = fixedAssetDept;
	}	

}
