package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="dept_it_asset")
public class DeptItAsset {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="it_asset_id")
	private String it_asset_id;
	
	@ManyToOne
	@JoinColumn(name = "dept_id", referencedColumnName = "dept_id",insertable=false, updatable=false)
	private Department departmentItAsset;
	
	@ManyToOne
	@JoinColumn(name = "it_asset_id", referencedColumnName = "it_asset_id",insertable=false, updatable=false)
	private ItAsset itAssetDept;

	
	public DeptItAsset()
	{
		
	}
	
	public DeptItAsset(int dept_id, String it_asset_id) {
		super();
		this.dept_id = dept_id;
		this.it_asset_id = it_asset_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getIt_asset_id() {
		return it_asset_id;
	}

	public void setIt_asset_id(String it_asset_id) {
		this.it_asset_id = it_asset_id;
	}

	public Department getDepartmentItAsset() {
		return departmentItAsset;
	}

	public void setDepartmentItAsset(Department departmentItAsset) {
		this.departmentItAsset = departmentItAsset;
	}

	public ItAsset getItAssetDept() {
		return itAssetDept;
	}

	public void setItAssetDept(ItAsset itAssetDept) {
		this.itAssetDept = itAssetDept;
	}
	
	
	

}
