package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="dept_project")
public class DeptProject {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="pj_id")
	private int pj_id;
	
	@ManyToOne
	@JoinColumn(name = "dept_id", referencedColumnName = "dept_id",insertable=false, updatable=false)
	private Department departmentPj;

	@OneToOne
	@JoinColumn(name = "pj_id", referencedColumnName = "pj_id",insertable=false, updatable=false)
	private Project projectDept;
	
	public DeptProject()
	{
		
	}
	
	public DeptProject(int dept_id, int pj_id) {
		super();
		
		this.dept_id = dept_id;
		this.pj_id = pj_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public int getPj_id() {
		return pj_id;
	}

	public void setPj_id(int pj_id) {
		this.pj_id = pj_id;
	}

	public Department getDepartmentPj() {
		return departmentPj;
	}

	public void setDepartmentPj(Department departmentPj) {
		this.departmentPj = departmentPj;
	}

	public Project getProjectDept() {
		return projectDept;
	}

	public void setProjectDept(Project projectDept) {
		this.projectDept = projectDept;
	}
	
	
}
