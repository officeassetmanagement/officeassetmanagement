package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="dept_tool_asset")
public class DeptToolAsset {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="tool_asset_id")
	private String tool_asset_id;
	
	@ManyToOne
	@JoinColumn(name = "dept_id", referencedColumnName = "dept_id",insertable=false, updatable=false)
	private Department departmentToolAsset;
	
	@ManyToOne
	@JoinColumn(name = "tool_asset_id", referencedColumnName = "tool_asset_id",insertable=false, updatable=false)
	private ToolAsset toolAssetDept;

	public DeptToolAsset()
	{
		
	}
	
	public DeptToolAsset( int dept_id, String tool_asset_id) {
		super();
		this.dept_id = dept_id;
		this.tool_asset_id = tool_asset_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getTool_asset_id() {
		return tool_asset_id;
	}

	public void setTool_asset_id(String tool_asset_id) {
		this.tool_asset_id = tool_asset_id;
	}

	public Department getDepartmentToolAsset() {
		return departmentToolAsset;
	}

	public void setDepartmentToolAsset(Department departmentToolAsset) {
		this.departmentToolAsset = departmentToolAsset;
	}

	public ToolAsset getToolAssetDept() {
		return toolAssetDept;
	}

	public void setToolAssetDept(ToolAsset toolAssetDept) {
		this.toolAssetDept = toolAssetDept;
	}
	
	
	
}
