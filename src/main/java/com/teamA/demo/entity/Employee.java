package com.teamA.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="employee")
public class Employee {
	
	@Id

	@Column(name="emp_id",nullable=false)
	private int emp_id;
	
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="emp_name")
	private String emp_name;
	
	@Column(name="role_name")
	private String role_name;
	
	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="dob")
	private Date dob;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="address")
	private String address;
	
	@Column(name="nation")
	private String nation;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="start_date")
	private Date start_date;
	
	@Column(name="end_date")
	private Date end_date;
	
	@Column(name="available")
	private boolean available;
	
	@ManyToOne
	@JoinColumn(name="dept_id", referencedColumnName="dept_id",insertable=false, updatable=false)
	private Department departmentEmp;
	
	@OneToMany(mappedBy = "employeePj")
	private List<ProjectAssign> projectAssignList;
	
	@OneToMany(mappedBy = "employeeAsset")
	private List<EmployeeAsset> employeeAssetList;
	
	@OneToMany(mappedBy = "empRequests")
	private List<Requests> requestsEmpList;
	
	public Employee() {
		
	}


	public List<EmployeeAsset> getEmployeeAssetList() {
		return employeeAssetList;
	}



	public void setEmployeeAssetList(List<EmployeeAsset> employeeAssetList) {
		this.employeeAssetList = employeeAssetList;
	}



	public List<Requests> getRequestsEmpList() {
		return requestsEmpList;
	}



	public void setRequestsEmpList(List<Requests> requestsEmpList) {
		this.requestsEmpList = requestsEmpList;
	}



	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_id) {
		this.role_name = role_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	
	
	public Date getStart_date() {
		return start_date;
	}


	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}


	public Date getEnd_date() {
		return end_date;
	}


	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}


	public boolean isAvailable() {
		return available;
	}


	public void setAvailable(boolean available) {
		this.available = available;
	}


	public Department getDepartmentEmp() {
		return departmentEmp;
	}

	public void setDepartmentEmp(Department departmentEmp) {
		this.departmentEmp = departmentEmp;
	}

	public List<ProjectAssign> getProjectAssignList() {
		return projectAssignList;
	}

	public void setProjectAssignList(List<ProjectAssign> projectAssignList) {
		this.projectAssignList = projectAssignList;
	}


	public void setEnd_date(java.util.Date end_date) {
		// TODO Auto-generated method stub
		
	}



	


	
}
