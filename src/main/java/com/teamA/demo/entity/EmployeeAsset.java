package com.teamA.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="employee_asset")
public class EmployeeAsset {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="emp_id",nullable=false)
	private int emp_id;
	
	@Column(name="it_asset_id")
	private String it_asset_id;
	
	@Column(name="use_date")
	private Date use_date;
	
	@ManyToOne(optional=false)
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id",insertable=false, updatable=false)
	private Employee employeeAsset;
	
	@ManyToOne
	@JoinColumn(name = "it_asset_id", referencedColumnName = "it_asset_id",insertable=false, updatable=false)
	private ItAsset itAssetEmp;
	
	public EmployeeAsset() {
		
	}

	public EmployeeAsset(int emp_id, String it_asset_id, Date use_date) {
		super();
		this.emp_id = emp_id;
		this.it_asset_id = it_asset_id;
		this.use_date = use_date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public String getIt_asset_id() {
		return it_asset_id;
	}

	public void setIt_asset_id(String it_asset_id) {
		this.it_asset_id = it_asset_id;
	}

	public Date getUse_date() {
		return use_date;
	}

	public void setUse_date(Date use_date) {
		this.use_date = use_date;
	}

	public Employee getEmployeeAsset() {
		return employeeAsset;
	}

	public void setEmployeeAsset(Employee employeeAsset) {
		this.employeeAsset = employeeAsset;
	}

	public ItAsset getItAssetEmp() {
		return itAssetEmp;
	}

	public void setItAssetEmp(ItAsset itAssetEmp) {
		this.itAssetEmp = itAssetEmp;
	}
	
	

}
