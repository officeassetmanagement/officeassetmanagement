package com.teamA.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="fixed_asset")
public class FixedAsset {

	@Id
	
	@Column(name="fixed_asset_id")
	private String fixed_asset_id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="available")
	private boolean available;
	
	@Column(name="purchase_date")
	private Date purchase_date;
	
	@Column(name="price")
	private int price;
	
	@OneToOne(mappedBy = "fixedAssetDept")
	private DeptFixedAsset deptFixedAssetList;	

	public FixedAsset(String fixed_asset_id, String name, boolean available, Date purchase_date, int price) {
		super();
		this.fixed_asset_id = fixed_asset_id;
		this.name = name;
		this.available = available;
		this.purchase_date = purchase_date;
		this.price = price;
	}

	public FixedAsset() {
	
	}

	public String getFixed_asset_id() {
		return fixed_asset_id;
	}

	public void setFixed_asset_id(String fixed_asset_id) {
		this.fixed_asset_id = fixed_asset_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Date getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public DeptFixedAsset getDeptFixedAssetList() {
		return deptFixedAssetList;
	}

	public void setDeptFixedAssetList(DeptFixedAsset deptFixedAssetList) {
		this.deptFixedAssetList = deptFixedAssetList;
	}
	
}
