package com.teamA.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="it_asset")
public class ItAsset {
	
	@Id
	@Column(name="it_asset_id")
	private String it_asset_id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="brand")
	private String brand;
	
	@Column(name="serial_no")
	private String serial_no;
	
	@Column(name="model")
	private String model;
	
	@Column(name="purchase_date")
	private Date purchase_date;
	
	@Column(name="price")
	private int price;
	
	@Column(name="available")
	private boolean available;
	
	@OneToMany(mappedBy = "itAssetEmp")
	private List<EmployeeAsset> itAssetEmpList;
	
	@OneToMany(mappedBy = "itAssetDept")
	private List<DeptItAsset> deptItAssetList;

	public ItAsset() {
		
	}

	public ItAsset(String it_asset_id, String type, String brand,String serial_no, String model, Date purchase_date, int price, boolean available) {
		super();
		this.it_asset_id = it_asset_id;
		this.type = type;
		this.brand = brand;
		this.serial_no = serial_no;
		this.model = model;
		this.purchase_date = purchase_date;
		this.price = price;
		this.available = available;
	}

	public String getIt_asset_id() {
		return it_asset_id;
	}

	public void setIt_asset_id(String it_asset_id) {
		this.it_asset_id = it_asset_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getSerial_no() {
		return serial_no;
	}

	public void setSerial_no(String serial_no) {
		this.serial_no = serial_no;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public List<EmployeeAsset> getItAssetEmpList() {
		return itAssetEmpList;
	}

	public void setItAssetEmpList(List<EmployeeAsset> itAssetEmpList) {
		this.itAssetEmpList = itAssetEmpList;
	}

	public List<DeptItAsset> getDeptItAssetList() {
		return deptItAssetList;
	}

	public void setDeptItAssetList(List<DeptItAsset> deptItAssetList) {
		this.deptItAssetList = deptItAssetList;
	}	
	
}
