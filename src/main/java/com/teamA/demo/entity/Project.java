package com.teamA.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pj_id")
	private int pj_id;

	@Column(name = "pj_name")
	private String pj_name;

	@Column(name = "pj_desc")
	private String pj_desc;

	@Column(name = "type")
	private String type;

	@Column(name = "location")
	private String location;

	@Column(name = "pj_customer")
	private String pj_customer;

	@Column(name = "start_date")
	private Date start_date;

	@Column(name = "end_date")
	private Date end_date;
		
	@Column(name="emp_id")
	private int emp_id;
	 	
	@OneToMany(mappedBy = "projectEmp")
	private List<ProjectAssign> projectAssignListEmp;
	
	@OneToMany(mappedBy = "projectDept")
	private List<DeptProject> deptProjectList;
	
	@OneToMany(mappedBy = "projectRequests")
	private List<Requests> requestsProjectList;

	public Project() {
		
	}
	  public Project(String pj_name,String pj_desc,String type,String location,String pj_customer,Date start_date,Date end_date,int emp_id) {
		  super();
		  this.pj_name=pj_name;
		  this.pj_desc=pj_desc;
		  this.type=type;
		  this.location=location;
		  this.pj_customer=pj_customer;
		  this.start_date=start_date;
		  this.end_date=end_date;
		 this.emp_id=emp_id;	  
	  }
	 
	public int getPj_id() {
		return pj_id;
	}

	public void setPj_id(int pj_id) {
		this.pj_id = pj_id;
	}

	public String getPj_name() {
		return pj_name;
	}

	public void setPj_name(String pj_name) {
		this.pj_name = pj_name;
	}

	public String getPj_desc() {
		return pj_desc;
	}

	public void setPj_desc(String pj_desc) {
		this.pj_desc = pj_desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPj_customer() {
		return pj_customer;
	}

	public void setPj_customer(String pj_customer) {
		this.pj_customer = pj_customer;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
		
	}
	
	public int getEmp_id(){ 
		return emp_id; 
	} 
	
	public void setEmp_id(int emp_id) {
	  this.emp_id=emp_id; 
	}
	 		
	public List<ProjectAssign> getProjectAssignListEmp() {
		return projectAssignListEmp;
	}
	
	public void setProjectAssignListEmp(List<ProjectAssign> projectAssignListEmp) {
		this.projectAssignListEmp = projectAssignListEmp;
	}
	
	public List<DeptProject> getDeptProjectList() {
		return deptProjectList;
	}
	
	public void setDeptProjectList(List<DeptProject> deptProjectList) {
		this.deptProjectList = deptProjectList;
	}
	
	public List<Requests> getRequestsProjectList() {
		return requestsProjectList;
	}
	public void setRequestsProjectList(List<Requests> requestsProjectList) {
		this.requestsProjectList = requestsProjectList;
	}	 
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "project [pj_id=" + pj_id + ", pj_name=" + pj_name + ", pj_desc=" + pj_desc + "," + " type=" + type
				+ ", location=" + location + ", pj_customer=" + pj_customer + ", start-date=" + start_date
				+ ", end_date=" + end_date +", emp_id=" + "]";
	}

}
