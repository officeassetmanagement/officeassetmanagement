package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="project_assign")
public class ProjectAssign {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="id")
	private int id;
	
	@Column(name="emp_id")
	private int emp_id;
	
	@Column(name="pj_id")
	private int pj_id;
	
	@ManyToOne
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id",insertable=false, updatable=false)
	private Employee employeePj;
	
	@ManyToOne
	@JoinColumn(name = "pj_id", referencedColumnName = "pj_id",insertable=false, updatable=false)
	private Project projectEmp;
	
	public ProjectAssign() {
		
	}
	
	public ProjectAssign(int emp_id,Integer pj_id) {
		super();
		this.emp_id=emp_id;
		this.pj_id=pj_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public int getPj_id() {
		return pj_id;
	}

	public void setPj_id(int pj_id) {
		this.pj_id = pj_id;
	}

	public Project getProjectEmp() {
		return projectEmp;
	}

	public void setProjectEmp(Project projectEmp) {
		this.projectEmp = projectEmp;
	}
	
	

	public Employee getEmployeePj() {
		return employeePj;
	}

	public void setEmployeePj(Employee employeePj) {
		this.employeePj = employeePj;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "project_assign [id=" + id + ", emp_id=" + emp_id + ", pj_id=" + pj_id + "]";
	}
	

}
