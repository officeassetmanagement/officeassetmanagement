package com.teamA.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="requests")
public class Requests {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="request_id")
	private int request_id;
	
	@Column(name="tool_set_id")
	private String tool_set_id;
	
	@Column(name="tool_asset_id",nullable=false)
	private String tool_asset_id;
	
	@Column(name="emp_id",nullable=false)
	private int emp_id;
	
	@Column(name="pj_id",nullable=false)
	private int pj_id;
	
	@Column(name="request_qty")
	private int request_qty;
	
	@Column(name="accept_qty",nullable=true)
	private int accept_qty;
	
	@Column(name="reject_qty")
	private int reject_qty;
	
	@Column(name="return_qty")
	private int return_qty;
	
	@Column(name="date_time")
	private Date date_time;
	
	@Column(name="acceptby")
	private String acceptby;
	
	@Column(name="rejectby",nullable = false)
	private String rejectby;
	
	@Column(name="out_date")
	private Date out_date;
	
	@Column(name="return_date")
	private Date return_date;

	@ManyToOne(optional=false)
	@JoinColumn(name = "tool_asset_id", referencedColumnName = "tool_asset_id",insertable=false, updatable=false)
	private ToolAsset toolAssetRequests;
	
	@ManyToOne(optional=false)
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id",insertable=false, updatable=false)
	private Employee empRequests;
	
	@ManyToOne(optional=false)
	@JoinColumn(name = "pj_id", referencedColumnName = "pj_id",insertable=false, updatable=false)
	private Project projectRequests;
	
	@OneToMany(mappedBy = "damageRequest")
	private List<ToolAssetDamage> toolAssetDamageList;
	
	public Requests()
	{
		
	}
	
	public Requests(String tool_set_id, String tool_asset_id, int emp_id, int pj_id, int request_qty,
			int accept_qty,int reject_qty,int return_qty, Date date_time,String acceptby,String rejectby,Date out_date,Date return_date) {
		super();
		
		this.tool_set_id = tool_set_id;
		this.tool_asset_id = tool_asset_id;
		this.emp_id = emp_id;
		this.pj_id = pj_id;
		this.request_qty = request_qty;
		this.accept_qty = accept_qty;
		this.reject_qty = reject_qty;
		this.return_qty = return_qty;
		this.date_time = date_time;	
		this.acceptby = acceptby;
		this.rejectby = rejectby;
		this.out_date = out_date;
		this.return_date = return_date;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public String getTool_set_id() {
		return tool_set_id;
	}

	public void setTool_set_id(String tool_set_id) {
		this.tool_set_id = tool_set_id;
	}

	public String getTool_asset_id() {
		return tool_asset_id;
	}

	public void setTool_asset_id(String tool_asset_id) {
		this.tool_asset_id = tool_asset_id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public int getPj_id() {
		return pj_id;
	}

	public void setPj_id(int pj_id) {
		this.pj_id = pj_id;
	}

	public Date getDate_time() {
		return date_time;
	}

	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}

	public String getAcceptby() {
		return acceptby;
	}

	public void setAcceptby(String acceptby) {
		this.acceptby = acceptby;
	}

	public String getRejectby() {
		return rejectby;
	}

	public void setRejectby(String rejectby) {
		this.rejectby = rejectby;
	}

	public Date getOut_date() {
		return out_date;
	}

	public void setOut_date(Date out_date) {
		this.out_date = out_date;
	}

	public Date getReturn_date() {
		return return_date;
	}

	public void setReturn_date(Date return_date) {
		this.return_date = return_date;
	}

	public int getRequest_qty() {
		return request_qty;
	}

	public void setRequest_qty(int request_qty) {
		this.request_qty = request_qty;
	}

	public int getAccept_qty() {
		return accept_qty;
	}

	public void setAccept_qty(int accept_qty) {
		this.accept_qty = accept_qty;
	}

	public int getReject_qty() {
		return reject_qty;
	}

	public void setReject_qty(int reject_qty) {
		this.reject_qty = reject_qty;
	}

	public int getReturn_qty() {
		return return_qty;
	}

	public void setReturn_qty(int return_qty) {
		this.return_qty = return_qty;
	}

	public ToolAsset getToolAssetRequests() {
		return toolAssetRequests;
	}

	public void setToolAssetRequests(ToolAsset toolAssetRequests) {
		this.toolAssetRequests = toolAssetRequests;
	}

	public Employee getEmpRequests() {
		return empRequests;
	}

	public void setEmpRequests(Employee empRequests) {
		this.empRequests = empRequests;
	}

	public Project getProjectRequests() {
		return projectRequests;
	}

	public void setProjectRequests(Project projectRequests) {
		this.projectRequests = projectRequests;
	}

	public List<ToolAssetDamage> getToolAssetDamageList() {
		return toolAssetDamageList;
	}

	public void setToolAssetDamageList(List<ToolAssetDamage> toolAssetDamageList) {
		this.toolAssetDamageList = toolAssetDamageList;
	}	
}
