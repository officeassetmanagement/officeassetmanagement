package com.teamA.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tool_asset")
public class ToolAsset {

	@Id
	@Column(name="tool_asset_id")
	private String tool_asset_id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="size")
	private String size;
	
	@Column(name="qty")
	private int qty;
	
	@Column(name="available_qty")
	private int available_qty;
	
	@Column(name="purchase_date")
	private Date purchase_date;
	
	@Column(name="price")
	private int price;
	
	@OneToMany(mappedBy = "toolAssetDept")
	private List<DeptToolAsset> deptToolAssetList;
	
	@OneToMany(mappedBy = "toolAssetRequests")
	private List<Requests> requestsToolAssetList;
		
	public ToolAsset() {
		
	}

	public ToolAsset(String tool_asset_id, String name, String size, int qty,int available_qty, Date purchase_date, int price) {
		super();
		this.tool_asset_id = tool_asset_id;
		this.name = name;
		this.size = size;
		this.qty = qty;
		this.available_qty = available_qty;
		this.purchase_date = purchase_date;
		this.price = price;
	}

	public String getTool_asset_id() {
		return tool_asset_id;
	}

	public void setTool_asset_id(String tool_asset_id) {
		this.tool_asset_id = tool_asset_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getAvailable_qty() {
		return available_qty;
	}

	public void setAvailable_qty(int available_qty) {
		this.available_qty = available_qty;
	}

	public Date getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public List<DeptToolAsset> getDeptToolAssetList() {
		return deptToolAssetList;
	}

	public void setDeptToolAssetList(List<DeptToolAsset> deptToolAssetList) {
		this.deptToolAssetList = deptToolAssetList;
	}

	public List<Requests> getRequestsToolAssetList() {
		return requestsToolAssetList;
	}

	public void setRequestsToolAssetList(List<Requests> requestsToolAssetList) {
		this.requestsToolAssetList = requestsToolAssetList;
	}

}
