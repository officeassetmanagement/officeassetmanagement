package com.teamA.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tool_asset_demage")
public class ToolAssetDamage {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tool_asset_demage_id")
	private int tool_asset_damage_id;
	
	@Column(name="request_id")
	private int request_id;
	
	@Column(name="qty")
	private int qty;
	
	@ManyToOne
	@JoinColumn(name = "request_id", referencedColumnName = "request_id",insertable=false, updatable=false)
	private Requests damageRequest;
	
	public ToolAssetDamage() {
		
	}
	
	public ToolAssetDamage(Integer request_id,Integer qty) {
		super();
		this.request_id=request_id;
		this.qty=qty;
	}
	
	public int getTool_asset_damage_id() {
		return tool_asset_damage_id;
	}
	
	public void setTool_asset_damage_id(int tool_asset_damage_id) {
		this.tool_asset_damage_id = tool_asset_damage_id;
	}
	
	public int getRequest_id() {
		return request_id;
	}
	
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	
	public int getQty() {
		return qty;
	}
	
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public Requests getDamageRequest() {
		return damageRequest;
	}
	
	public void setDamageRequest(Requests damageRequest) {
		this.damageRequest = damageRequest;
	}
	
}
