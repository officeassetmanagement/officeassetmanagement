package com.teamA.demo.model;

public class employee {
	private String emp_id;
	private String emp_name;
	private int role_id;
	private String email;
	private String dob;
	private String phone;
	private String gender;
public employee() {
		
	}
public employee(String emp_id, String emp_name, int role_id, String email, String dob,
		String phone,String gender) {
	super();
	this.emp_id = emp_id;
	
	this.emp_name = emp_name;
	this.role_id = role_id;
	this.email = email;
	
	this.dob = dob;
	this.phone = phone;
	
	
	this.gender = gender;
	
	
}
public String getEmp_id() {
	return emp_id;
}
public void setEmp_id(String emp_id) {
	this.emp_id = emp_id;
}
public String getEmp_name() {
	return emp_name;
}
public void setEmp_name(String emp_name) {
	this.emp_name = emp_name;
}
public int getRole_id() {
	return role_id;
}
public void setRole_id(int role_id) {
	this.role_id = role_id;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getDob() {
	return dob;
}
public void setDob(String dob) {
	this.dob = dob;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}

}
