package com.teamA.demo.service;

import java.util.List;

import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;

public interface AdminService {
	public List<Project> findAll();
	public void addNewDept(Department theDepartment);
	List<Employee> DeptMemberList(int dept_id);
	public List<DeptFixedAsset> DeptFixedAsset(int dept_id);
	public List<DeptItAsset> DeptItAsset(int dept_id);
	public List<DeptProject> DeptProject(int dept_id);
	
	List<Employee> findAllEmployee();
	List<Department> findAllDept();
	void addEmployee(Employee theEmployee);
	Employee findById(int emp_id);
	
	List<ProjectAssign> showMemberList(int pj_id);
	public List<FixedAsset> findAllFixedAsset();
	public List<ItAsset> findAllItAsset();
	public List<ToolAsset> findAllToolAsset();
	


}
