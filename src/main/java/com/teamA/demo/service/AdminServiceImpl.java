package com.teamA.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.teamA.demo.dao.AdminDao;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.ToolAsset;

@Service
public class AdminServiceImpl implements AdminService{

	

	private AdminDao adminDao;
	
	
	@Autowired
	 public AdminServiceImpl(AdminDao theAdminDao) {
		adminDao=theAdminDao;
	}
	

	@Override
	@Transactional
	public List<Project> findAll() {
		return adminDao.findAll();
	}


	@Override
	@Transactional
	public List<Employee> findAllEmployee() {
		return adminDao.findAllEmployee();
	}


	@Override
	@Transactional
	public List<Department> findAllDept() {
		return adminDao.findAllDept();
	}

	@Override
	@Transactional
	public Employee findById(int emp_id) {
		return adminDao.findById(emp_id);
	}

	@Override
	@Transactional
	public void addEmployee(Employee theEmployee) {
		adminDao.addEmployee(theEmployee);
	}
	

	@Override
	@Transactional
	public void addNewDept(Department theDepartment) {
		adminDao.addNewDept(theDepartment);
		
	}
	
	@Override
	@Transactional
	public List<ProjectAssign> showMemberList(int pj_id) {
		List<ProjectAssign> projectListForMember = adminDao.showMemberList(pj_id);
		return projectListForMember;
	}
	
	@Override
	public List<FixedAsset> findAllFixedAsset() {
		return adminDao.findAllFixedAsset();
	}


	@Override
	public List<ItAsset> findAllItAsset() {
		return adminDao.findAllItAsset();
	}


	@Override
	public List<ToolAsset> findAllToolAsset() {
		return adminDao.findAllToolAsset();

	}


	@Override
	public List<Employee> DeptMemberList(int dept_id) {
		
		return adminDao.showDeptMember(dept_id);
	}

	@Override
	public List<com.teamA.demo.entity.DeptFixedAsset> DeptFixedAsset(int dept_id) {
		// TODO Auto-generated method stub
		return adminDao.DeptFixedAsset(dept_id);
	}


	@Override
	public List<com.teamA.demo.entity.DeptItAsset> DeptItAsset(int dept_id) {
		// TODO Auto-generated method stub
		return adminDao.DeptItAsset(dept_id);
	}


	@Override
	public List<com.teamA.demo.entity.DeptProject> DeptProject(int dept_id) {
		// TODO Auto-generated method stub
		return adminDao.DeptProject(dept_id);
	}
}
