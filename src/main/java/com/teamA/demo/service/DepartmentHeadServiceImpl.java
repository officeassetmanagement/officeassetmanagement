package com.teamA.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamA.demo.dao.DepartmentHeadDao;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;

@Service
public  class DepartmentHeadServiceImpl implements DepartmentHeadService {
	
	private DepartmentHeadDao departmentheadDao;
	
	@Autowired
	public DepartmentHeadServiceImpl(DepartmentHeadDao departmentHeadDao) {
		departmentheadDao = departmentHeadDao;
	}

	@Override
	@Transactional
	public List<DeptProject> findProjectForEachDepartment(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findProjectForEachDepartment(dept_id);
	}

	@Override
	@Transactional
	public List<Employee> findEmployeeList(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findEmployeeList(dept_id);
	}

	@Override
	@Transactional
	public List<Requests> findAccept(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findAccept(dept_id);
	}

	@Override
	@Transactional
	public List<Requests> findReject(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findReject(dept_id);
	}

    //for asset
	
	@Override
	@Transactional
	public List<DeptFixedAsset> findFixedAsset(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findFixedAsset(dept_id);
	}

	@Override
	@Transactional
	public List<DeptItAsset> findAllItAsset(int dept_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.findAllItAsset(dept_id);
	}

	@Override
	@Transactional
	public List<Requests> ReuestToolList(String tool_set_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.ReuestToolList(tool_set_id);
	}

	@Override
	@Transactional
	public List<Requests> AcceptViewTools(int pj_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.AcceptViewTools(pj_id);
	}

	@Override
	public List<Requests> findPendingProject(int dept_id) {
		return departmentheadDao.findPendingProject(dept_id);
	}

	@Override
	public List<Requests> RejectViewTools(int pj_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.RejectViewTools(pj_id);
	}

	@Override
	public List<Requests> RequestToolLists(String tool_set_id) {
		// TODO Auto-generated method stub
		return departmentheadDao.RequestToolLists(tool_set_id);
	}

	@Override
	public void AcceptHistoryTool(int request_id) {
		// TODO Auto-generated method stub
		departmentheadDao.AcceptHistoryTool(request_id);
		
	}

	@Override
	public void RejectHistoryTool(int request_id) {
		// TODO Auto-generated method stub
		departmentheadDao.RejectHistoryTool(request_id);
	}
	 
	
}
