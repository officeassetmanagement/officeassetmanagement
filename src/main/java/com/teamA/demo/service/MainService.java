package com.teamA.demo.service;

import java.util.List;

import com.teamA.demo.entity.Employee;

public interface MainService {

	public List<Employee> findUser();

}
