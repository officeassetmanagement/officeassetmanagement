package com.teamA.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamA.demo.dao.MainDao;
import com.teamA.demo.entity.Employee;

@Service
public class MainServiceImpl implements MainService {
	private MainDao mainDao;
	
	@Autowired
	public MainServiceImpl(MainDao maDao) {
		mainDao=maDao;
	}
	
	@Override
	@Transactional
	public List<Employee> findUser() {
		return mainDao.findUser();
	}
	
	

}
