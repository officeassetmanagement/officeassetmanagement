package com.teamA.demo.service;

import java.util.List;

import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;

public interface MemberService {
	public List<ProjectAssign> showProjectForMember(int emp_id);
	
	public Project showProjectDetail(int pj_id);

	public List<Requests> showPending(int emp_id);
	
	public List<Requests> showAccept(int emp_id);
	
	public List<Requests> showReject(int emp_id);
	
	public Employee showOperationManager(int roleName);
	
	public List<Requests> showRequests(int pj_id);
	
	public List<Requests> showAcceptProject(int pj_id);
	
	public List<Requests> showRejectProject(int pj_id);
	
	public List<ToolAsset> showToolAssetList();
	
	public void saveRequest(Requests req);
	
	public List<Requests>showAllRequest();
	
	public List<Requests> showRequestTools(String ts_id);

	public void removeById(int reqId);

	public void removeRequest(String tsId);

	public Requests getRequestTool(String toolId, int pj_ID);

}
