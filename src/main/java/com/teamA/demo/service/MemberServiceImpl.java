package com.teamA.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.teamA.demo.dao.MemberDao;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;

@Service
public class MemberServiceImpl implements MemberService{
	
	private MemberDao memberDao;
	
	@Autowired
	public MemberServiceImpl(MemberDao theMemberDao) {
		memberDao=theMemberDao;
	}

	@Override
	@Transactional
	public List<ProjectAssign> showProjectForMember(int emp_id) {
		List<ProjectAssign> projectListForMember = memberDao.showProjectForMember(emp_id);
		return projectListForMember;
	}
	public void removeRequest(String tsId) {
		memberDao.removeRequest(tsId);
	}
	public Requests getRequestTool(String toolId, int pj_ID) {
		return memberDao.getRequestTool(toolId,pj_ID);
	}

	@Override
	@Transactional
	public Project showProjectDetail(int pj_id) {
		
		return memberDao.showProjectDetail(pj_id);
	}
	public void removeById(int reqId) {
		memberDao.removeById(reqId);
	}
	@Override
	public List<Requests> showPending(int emp_id) {
		List<Requests> pendings = memberDao.showPending(emp_id);
		return pendings;
	}

	@Override
	public List<Requests> showAccept(int emp_id) {
		List<Requests> accept = memberDao.showAccept(emp_id);
		return accept;
	}

	@Override
	public Employee showOperationManager(int roleName) {
		Employee op= memberDao.showOperationManager(roleName);
		return op;
	}

	@Override
	public List<Requests> showReject(int emp_id) {
		List<Requests> rejects = memberDao.showReject(emp_id);
		return rejects;
	}

	@Override
	public List<Requests> showRequests(int pj_id) {
		List<Requests> requests = memberDao.showRequests(pj_id);
		return requests;
	}

	@Override
	public List<Requests> showAcceptProject(int pj_id) {
		List<Requests> acceptProject = memberDao.showAcceptProject(pj_id);
		return acceptProject;
	}

	@Override
	public List<Requests> showRejectProject(int pj_id) {
		List<Requests> rejectProject = memberDao.showRejectProject(pj_id);
		return rejectProject;
	}

	@Override
	public List<ToolAsset> showToolAssetList() {
		List<ToolAsset> deptToolAsset = memberDao.showToolAssetList();
		return deptToolAsset;
	}

	@Override
	public void saveRequest(Requests req) {
		memberDao.saveRequest(req);
	}
	
	@Override
	public List<Requests> showAllRequest(){
		List<Requests> allRequest = memberDao.showAllRequest();
		return allRequest;
	}
	
	@Override
	public List<Requests> showRequestTools(String ts_id){
		List<Requests> requestTools = memberDao.showRequestTools(ts_id);
		return requestTools;
	}


}
