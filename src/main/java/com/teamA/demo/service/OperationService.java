package com.teamA.demo.service;

import java.util.Date;
import java.util.List;

import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.EmployeeAsset;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.ToolAssetDamage;

public interface OperationService {
	
	
	public List<FixedAsset> findAll();

	public void save(FixedAsset fixedAsset);
	
	//public void saveItAsset(ItAsset asset);

	public void deleteById(String fixed_asset_id);

	public FixedAsset findById(String fixed_asset_id);

	public List<ItAsset> findAllIT();

	public void save(ItAsset asset);

	public ItAsset findByITId(String it_asset_id);

	public void deleteByITId(String it_asset_id);

	public List<ToolAsset> findAllTool();

	public void save(ToolAsset toolAsset);

	public ToolAsset findToolById(String tool_asset_id);

	public void deleteToolById(String tool_asset_id);

	public List<ToolAssetDamage> getDamageTools();

	public List<Requests> getRequest();

	public List<Requests> findAccept(String acceptby);

	public List<Requests> findReject(String rejectby);

	public Department findDeptById(int id);

	public List<Requests> rejectView(int pj_id);

	public List<Requests> acceptView(int pj_id);

	public List<Requests> pendingView(String ts_id);

	public int findId(String tool_id, String tool_set_id);

	public void saveDamage(ToolAssetDamage damage);

	public void saveDeptFixedAsset(DeptFixedAsset asset);

	public List<DeptFixedAsset> getAllDeptFixedAsset();

	public void saveDeptItAsset(DeptItAsset asset);

	public List<DeptItAsset> getAllDeptItAsset();

	public void saveDeptToolAsset(DeptToolAsset asset);

	public void acceptById(int req_id);

	public void rejectById(int req_id); 
	
	public Requests getRequestById(int id);

	public void returnTools(int request_id,int return_qty, Date currentDate);

	public List<Employee> getAllEmployee();

	public List<ItAsset> getDevices();

	public void saveUserDevice(EmployeeAsset empAsset);

	public List<EmployeeAsset> getUsedDevices();

	public List<Requests> showRequestsByToolId(String tool_asset_id);

	public List<Requests> showHistory();

	public List<EmployeeAsset> showUserDevices(int empId);

	public void removeUserDevice(int id);
	
	public EmployeeAsset getEmployeeAssetById(int id);

	public void removeDeptItAsset(int dept_id, String it_asset_id);

	public List<EmployeeAsset> showUsers(String tool_asset_id);

	public void takenTools(List<Requests> acceptList, Date date);
	
}
