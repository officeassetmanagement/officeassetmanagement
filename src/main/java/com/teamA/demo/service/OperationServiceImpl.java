package com.teamA.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.teamA.demo.dao.OperationDao;
import com.teamA.demo.entity.Department;
import com.teamA.demo.entity.DeptFixedAsset;
import com.teamA.demo.entity.DeptItAsset;
import com.teamA.demo.entity.DeptToolAsset;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.EmployeeAsset;
import com.teamA.demo.entity.FixedAsset;
import com.teamA.demo.entity.ItAsset;
import com.teamA.demo.entity.Requests;
import com.teamA.demo.entity.ToolAsset;
import com.teamA.demo.entity.ToolAssetDamage;

@Service
public class OperationServiceImpl implements OperationService{
	
private OperationDao opDao;
	
	@Autowired
	public OperationServiceImpl(OperationDao operationDao) {
		opDao = operationDao;
	}

	@Override
	public List<FixedAsset> findAll() {
	
		return opDao.findAll();
	}

	@Override
	@Transactional
	public void save(FixedAsset fixedAsset) {
		opDao.save(fixedAsset);
		
	}
	public List<Employee> getAllEmployee(){
		return opDao.getAllEmployee();
	}
	public List<ItAsset> getDevices(){
		return opDao.getDevices();
	}

	@Override
	@Transactional
	public void save(ItAsset asset) {
		opDao.save(asset);
		
	}
	public void acceptById(int req_id) {
		opDao.acceptById(req_id);
	}

	@Override
	@Transactional
	public void deleteById(String fixed_asset_id) {
		opDao.deleteById(fixed_asset_id);		
	}

	@Override
	public FixedAsset findById(String fixed_asset_id) {
		return opDao.findById(fixed_asset_id);
	}

	@Override
	public List<ItAsset> findAllIT() {
		List<ItAsset> list = opDao.findAllIT();
		return list;
	}
	
	@Override
	public ItAsset findByITId(String it_asset_id) {
		return opDao.findByITId(it_asset_id);
	}

	@Override
	@Transactional
	public void deleteByITId(String it_asset_id) {
		opDao.deleteByITId(it_asset_id);
		
	}

	@Override
	public List<ToolAsset> findAllTool() {
		// TODO Auto-generated method stub
		return opDao.findAllTool();
	}

	@Override
	@Transactional
	public void save(ToolAsset toolAsset) {
		opDao.save(toolAsset);
		
	}

	@Override
	public ToolAsset findToolById(String tool_asset_id) {
		// TODO Auto-generated method stub
		return opDao.findToolById(tool_asset_id);
	}

	@Override
	@Transactional
	public void deleteToolById(String tool_asset_id) {
		// TODO Auto-generated method stub
		opDao.deleteToolById(tool_asset_id);
		
	}

	@Override
	public List<ToolAssetDamage> getDamageTools() {
		// TODO Auto-generated method stub
		return opDao.getDamageTools();
	}

	@Override
	public List<Requests> getRequest() {
		// TODO Auto-generated method stub
		return opDao.getRequest();
	}
	public void rejectById(int req_id) {
		opDao.rejectById(req_id);
	}

	
	@Override
	@Transactional
	public List<Requests> findAccept(String acceptby) {
		// TODO Auto-generated method stub
		return opDao.findAccept(acceptby);
	}

	@Override
	public List<Requests> findReject(String rejectby) {
		// TODO Auto-generated method stub
		return opDao.findReject(rejectby);
	}
	
	@Override
	public Department findDeptById(int id) {
		return opDao.findDeptById(id);
	}

	@Override
	public List<Requests> rejectView(int pj_id) {
		// TODO Auto-generated method stub
		return opDao.rejectView(pj_id);
	}

	@Override
	public List<Requests> acceptView(int pj_id) {
		// TODO Auto-generated method stub
		return opDao.acceptView(pj_id);
	}

	@Override
	public List<Requests> pendingView(String ts_id) {
		// TODO Auto-generated method stub
		return opDao.pendingView(ts_id);
	}

	@Override
	public int findId(String tool_id, String tool_set_id) {
		// TODO Auto-generated method stub
		return opDao.findId(tool_id,tool_set_id);
	}

	@Override
	public void saveDamage(ToolAssetDamage damage) {
		// TODO Auto-generated method stub
		opDao.saveDamage(damage);
		
	}

	@Override
	public void saveDeptFixedAsset(DeptFixedAsset asset) {
		opDao.saveDeptFixedAsset(asset);
	}

	@Override
	public List<DeptFixedAsset> getAllDeptFixedAsset() {
		return opDao.getAllDeptFixedAsset();
	}

	@Override
	public void saveDeptItAsset(DeptItAsset asset) {
		opDao.saveDeptItAsset(asset);
	}

	@Override
	public List<DeptItAsset> getAllDeptItAsset() {
		return opDao.getAllDeptItAsset();
	}

	@Override
	public void saveDeptToolAsset(DeptToolAsset asset) {
		opDao.saveDeptToolAsset(asset);
	}

	@Override
	public Requests getRequestById(int id) {
		return opDao.getRequestById(id);
	}

	@Override
	public void returnTools(int request_id,int return_qty, Date currentDate) {
		opDao.returnTools(request_id,return_qty,currentDate);
	}

	@Override
	public void saveUserDevice(EmployeeAsset empAsset) {
		opDao.saveUserDevice(empAsset);
	}

	@Override
	public List<EmployeeAsset> getUsedDevices() {
		return opDao.getUsedDevices();
	}

	@Override
	public List<Requests> showRequestsByToolId(String tool_asset_id) {
		return opDao.showRequestsById(tool_asset_id);
	}

	@Override
	public List<Requests> showHistory() {
		return opDao.showHistory();
	}

	@Override
	public List<EmployeeAsset> showUserDevices(int empId) {
		return opDao.showUserDevices(empId);
	}

	@Override
	public void removeUserDevice(int id) {
		opDao.removeUserDevice(id);
	}
	
	@Override
	public EmployeeAsset getEmployeeAssetById(int id) {
		return opDao.getEmployeeAssetById(id);
	}

	@Override
	public void removeDeptItAsset(int dept_id, String it_asset_id) {
		opDao.removeDeptItAsset(dept_id,it_asset_id);
	}

	@Override
	public List<EmployeeAsset> showUsers(String it_asset_id) {
		return opDao.showUsers(it_asset_id);
	}

	@Override
	public void takenTools(List<Requests> acceptList, Date date) {
		opDao.takenTools(acceptList,date);
	};

}
