package com.teamA.demo.service;

import java.util.List;

import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;

public interface ProjectManagerService {
	public List<Project> getAllProjectList(int emp_id);

	public void save(Project project);
	

	public Employee findById(int emp_id);

	public List<ProjectAssign> MemberInProject(int pj_id);

	public List<Employee> getEmployeeList(int dept_id);

	public void savePjAssign(ProjectAssign pjAssign);

	//public void deleteById(int pj_id, int emp_id);

	public void deleteById(int empid,int pj_id);

	public List<Requests> getAllRequest(String accept_id);

	public List<Project> getCompletePj(int emp_id);

	public List<Requests> getRequestList(int emp_id);

	public void saveProfile(Employee employee);

	public List<Requests> getAcceptList(int emp_id);

	public void acceptTool( int request_id);

	public List<Requests> getAcceptTool(int acceptToolId);

	public List<Requests> getRejectList(int emp_id);

	public List<Requests> getRejectTool(int rejectToolId);

	public void rejectTool(int request_id);

	public void saveDeptProject(DeptProject dept_project);

	public void endProject(int pjid);

	public List<Requests> getPjTool(int pj_id);

	/* public void deletePjById(int pj_id) */;

}
