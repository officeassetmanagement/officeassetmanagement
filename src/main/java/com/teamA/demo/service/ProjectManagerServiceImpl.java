package com.teamA.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamA.demo.dao.ProjectManagerDao;
import com.teamA.demo.entity.DeptProject;
import com.teamA.demo.entity.Employee;
import com.teamA.demo.entity.Project;
import com.teamA.demo.entity.ProjectAssign;
import com.teamA.demo.entity.Requests;

@Service
public class ProjectManagerServiceImpl implements ProjectManagerService {
	private ProjectManagerDao pmDao;
	
	@Autowired
	public ProjectManagerServiceImpl(ProjectManagerDao thepmDao) {
		pmDao=thepmDao;
	}
	@Override
	@Transactional
	public List<Project> getAllProjectList(int emp_id){
		return pmDao.getAllProject(emp_id);
	}
	
	public List<Project> getCompletePj(int emp_id){
		return pmDao.getCompletePj(emp_id);
	}
	public void save(Project project) {
		pmDao.save(project);		
	}
	public void endProject(int pjid) {
		pmDao.endProject(pjid);
	}
	public void saveDeptProject(DeptProject dept_project) {
		pmDao.saveDeptProject(dept_project);
	}
	public void saveProfile(Employee employee) {
		pmDao.saveProfile(employee);
	}
	
	 public void deleteById(int id,int pjid) {
		 pmDao.deleteById(id,pjid);
	}
	 
	@Override
	public Employee findById(int emp_id){
		return pmDao.findById(emp_id);
	}
	
	@Override
	public List<ProjectAssign> MemberInProject(int pj_id){
		return pmDao.MemberInProject(pj_id);
	}
	
	@Override
	public List<Employee> getEmployeeList(int dept_id){
		return pmDao.getEmployeeList(dept_id);
	}
	public List<Requests> getPjTool(int pj_id){
		return pmDao.getPjTool(pj_id);
	}
	
	@Override
	public void savePjAssign(ProjectAssign pjAssign) {
		pmDao.savePjAssign(pjAssign);
	}
	
	public List<Requests> getRequestList(int emp_id){
		return pmDao.getRequestList(emp_id);
	}
	public List<Requests> getAcceptList(int emp_id){
		return pmDao.getAcceptList(emp_id);
	}
	
	
	public List<Requests> getAllRequest(String accept_id){
		return pmDao.getAllRequest(accept_id);
	}
	public void acceptTool(int request_id) {
		pmDao.acceptTool(request_id);
	}
	public void rejectTool(int request_id) {
		pmDao.rejectTool(request_id);
	}
	public List<Requests> getAcceptTool(int acceptToolId){
		return pmDao.getAcceptTool(acceptToolId);
	}
	public List<Requests> getRejectList(int emp_id){
		return pmDao.getRejectList(emp_id);
	}
	public List<Requests> getRejectTool(int rejectToolId){
		return pmDao.getRejectTool(rejectToolId);
	}
	
}
